<?php

/**
 * @file
 * Provide views data and handlers for custom_teasers_views.module.
 */

/**
 * Implements hook_views_data().
 */
function service_links_views_data() {
  $data['node']['service_links'] = [
    'title' => t('Service links'),
    'help' => t('Display links to social sharing websites like Digg, del.icio.us, reddit, Technorati etc..'),
    'field' => [
      'id' => 'service_links',
    ],
  ];

  return $data;
}
