<?php

namespace Drupal\service_links;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Service link plugins.
 */
abstract class ServiceLinkBase extends PluginBase implements ServiceLinkInterface {

  /**
   * Provide details of the service.
   *
   * @return array
   *   The details of the service.
   */
  abstract public function getDetails();

}
