<?php

namespace Drupal\service_links\Element;

use Drupal\Core\Render\Element\RenderElement;

/**
 * Provides a service label element in the admin list of links.
 *
 * @RenderElement("service_label")
 */
class ServiceLabel extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#theme' => 'service_label',
      '#label' => 'Service',
      '#description' => 'Default Service Description',
      '#pre_render' => [
        [$class, 'preRenderServiceLabel'],
      ],
    ];
  }

  /**
   * Prepare the render array for the template.
   */
  public static function preRenderServiceLabel($element) {
    // Create a link render array using our #label.
    $element += [
      '#label' => 'Default label',
      '#icon' => FALSE,
      '#theme' => 'service_label',
    ];

    return $element;
  }

}
