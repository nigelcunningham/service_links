<?php

namespace Drupal\service_links\Element;

use Drupal\Core\Render\Element\RenderElement;

/**
 * Provides a service links element.
 *
 * @RenderElement("service_links")
 */
class ServiceLinks extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#theme' => 'service_links',
      '#page_entity' => NULL,
      '#label' => 'Service Links',
      '#description' => 'Links allowing a user to quickly and easily bookmark a page',
      '#pre_render' => [
        [$class, 'preRenderServiceLinks'],
      ],
    ];
  }

  /**
   * Prepare the render array for the template.
   */
  public static function preRenderServiceLinks($element) {
    // Create a link render array using our #label.
    $element += [
      '#label' => 'Bookmark this page using the following services',
      '#page_entity' => NULL,
      '#icon' => FALSE,
    ];

    switch ($element['#style']) {
      case 'fisheye':
        $element['#theme'] = 'service_links_fisheye';
        break;

      default:
        $element['#theme'] = 'service_links';
        break;
    }

    return $element;
  }

}
