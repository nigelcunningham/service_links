<?php

namespace Drupal\service_links\Element;

use Drupal\Core\Render\Element\RenderElement;
use Drupal\service_links\Controller\ServiceLinksController;

/**
 * Provides a single service link.
 *
 * @RenderElement("service_link")
 */
class ServiceLink extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#theme' => 'service_link',
      '#label' => 'Service Link',
      '#description' => 'A single link, allowing a user to quickly and easily bookmark a page',
      '#pre_render' => [
        [$class, 'preRenderServiceLink'],
      ],
    ];
  }

  /**
   * Prepare the render array for the template.
   */
  public static function preRenderServiceLink($element) {
    // Create a link render array using our #label.
    $element += [
      '#icon' => FALSE,
      '#theme' => 'service_link',
    ];

    switch ($element['#style']) {
      case ServiceLinksController::SERVICE_LINKS_STYLE_TEXT:
        $element['#theme'] = 'service_link_text';
        break;

      case ServiceLinksController::SERVICE_LINKS_STYLE_IMAGE:
        $element['#theme'] = 'service_link_image';
        break;

      case ServiceLinksController::SERVICE_LINKS_STYLE_IMAGE_AND_TEXT:
        $element['#theme'] = 'service_link_text_and_image';
        break;

      case ServiceLinksController::SERVICE_LINKS_STYLE_EMPTY:
        $element['#theme'] = 'service_link_empty';
        break;

      case ServiceLinksController::SERVICE_LINKS_STYLE_FISHEYE:
        $element['#theme'] = 'service_link_fisheye';
        break;
    }
    return $element;
  }

}
