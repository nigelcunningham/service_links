<?php

namespace Drupal\service_links\Controller;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Controller\TitleResolver;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Core\Entity\Entity;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Path\AliasManager;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Path\PathMatcher;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use GuzzleHttp\Exception\RequestException;
use Drupal\Component\Utility\Html;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

/**
 * Controller to make library functions available to various consumers.
 */
class ServiceLinksController extends ControllerBase implements ContainerInjectionInterface {

  const SERVICE_LINKS_STYLE_NONE = 0;

  const SERVICE_LINKS_STYLE_TEXT = 1;

  const SERVICE_LINKS_STYLE_IMAGE = 2;

  const SERVICE_LINKS_STYLE_IMAGE_AND_TEXT = 3;

  const SERVICE_LINKS_STYLE_EMPTY = 4;

  const SERVICE_LINKS_STYLE_FISHEYE = 5;

  const SERVICE_LINKS_SHORT_URL_USE_NEVER = 0;

  const SERVICE_LINKS_SHORT_URL_USE_WHEN_REQUESTED = 1;

  const SERVICE_LINKS_SHORT_URL_USE_ALWAYS = 2;

  const SERVICE_LINKS_SHORT_URL_TYPE_ENTITY = 1;

  const SERVICE_LINKS_SHORT_URL_TYPE_SERVICE = 2;

  const SERVICE_LINKS_SHORT_URL_TYPE_REDIRECT_DOMAIN = 3;

  const SERVICE_LINKS_SHORT_URL_TYPE_REDIRECT_ALL = 4;

  const SERVICE_LINKS_TAG_TITLE_ENTITY = 0;

  const SERVICE_LINKS_TAG_TITLE_OVERRIDE = 1;

  const SERVICE_LINKS_TAG_TITLE_TOKEN = 2;

  /**
   * Module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Config Factory.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPathStack;

  /**
   * Config Factory.
   *
   * @var \Drupal\Core\Path\AliasManager
   */
  protected $aliasManager;

  /**
   * The title resolver.
   *
   * @var \Drupal\Core\Controller\TitleResolverInterface
   */
  protected $titleResolver;

  /**
   * The path matcher.
   *
   * @var \Drupal\Core\Path\PathMatcher
   */
  protected $pathMatcher;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The Url Generator.
   *
   * @var \Drupal\Core\Routing\UrlGenerator
   */
  protected $urlGenerator;

  /**
   * Cache for $settings array.
   *
   * @var array
   */
  protected $settings;

  /**
   * Cache for settings key.
   *
   * @var string
   */
  protected $settingsKey;

  /**
   * Formatter settings array.
   *
   * @var array
   */
  protected $formatterSettings;

  /**
   * Service Link paths.
   *
   * @var array
   */
  private $sl_paths;

  /**
   * Service Link paths.
   *
   * @var array
   */
  private $sl_checkpath;

  /**
   * Constructs a new controller.
   *
   * @param \Drupal\Core\Extension\ModuleHandler $moduleHandler
   *   The module handler.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory.
   * @param \Drupal\Core\Path\CurrentPathStack $currentPathStack
   *   The current path for the current request.
   * @param \Drupal\Core\Path\AliasManager $aliasManager
   *   Path alias manager.
   * @param \Drupal\Core\Controller\TitleResolver $titleResolver
   *   The title resolver.
   * @param \Drupal\Core\Path\PathMatcher $pathMatcher
   *   The patch matcher service.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The account interface.
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $urlGenerator
   *   The url Generator interface.
   * @param string $settingsKey
   *   The settings key to use.
   * @param array $formatterSettings
   *   The array of settings from the field formatter / block / view.
   */
  public function __construct(
    ModuleHandler $moduleHandler,
    ConfigFactory $configFactory,
    CurrentPathStack $currentPathStack,
    AliasManager $aliasManager,
    TitleResolver $titleResolver,
    PathMatcher $pathMatcher,
    AccountProxyInterface $currentUser,
    UrlGeneratorInterface $urlGenerator,
    $settingsKey = 'service_links.settings',
    $formatterSettings = []
  ) {
    $this->moduleHandler = $moduleHandler;
    $this->configFactory = $configFactory;
    $this->currentPathStack = $currentPathStack;
    $this->aliasManager = $aliasManager;
    $this->titleResolver = $titleResolver;
    $this->pathMatcher = $pathMatcher;
    $this->currentUser = $currentUser;
    $this->urlGenerator = $urlGenerator;
    $this->settingsKey = $settingsKey;
    $this->formatterSettings = $formatterSettings;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $settingsKey = 'service_links.settings', $formatterSettings = []) {
    return new static(
      $container->get('module_handler'),
      $container->get('config.factory'),
      $container->get('path.current'),
      $container->get('path.alias_manager'),
      $container->get('title_resolver'),
      $container->get('path.matcher'),
      $container->get('current_user'),
      $container->get('url_generator'),
      $settingsKey,
      $formatterSettings
    );
  }

  /**
   * Expand the path around a filename depending from the context.
   *
   * @param string $filename
   *   If NULL the function return only the path with trailing slash.
   * @param string $context
   *   Define what path should consider this function.
   * @param string $default
   *   Concerning the image path is useful define a default path if
   *   the alternative is not set up.
   *
   * @return string
   *   A string with the full filename or the path.
   */
  public function expandPath($filename = NULL, $context = 'icons', $default = 'preset') {
    $module_path = '/' . $this->moduleHandler->getModule('service_links')
      ->getPath();

    if (strpos($filename, '/') !== FALSE) {
      return $filename;
    }

    if (!isset($this->sl_paths)) {
      $this->sl_paths['base'] = $module_path;

      $this->sl_paths += [
        'preset' => $this->sl_paths['base'] . '/images',
        'javascript' => $this->sl_paths['base'] . '/js',
        'css' => $this->sl_paths['base'] . '/css',
      ];

      $this->sl_checkpath = [
        'preset' => FALSE,
        'javascript' => FALSE,
        'css' => FALSE,
      ];
    }

    if (!isset($this->sl_paths[$context])) {
      $config = $this->configFactory->getEditable($this->settingsKey);
      $this->sl_paths[$context] = $config->get("path_{$context}");
      if (empty($this->sl_paths[$context])) {
        $this->sl_paths[$context] = $this->sl_paths[$default];
      }

      $this->sl_checkpath[$context] = $config->get("check_{$context}");
      if (empty($this->sl_checkpath[$context])) {
        $this->sl_checkpath[$context] = FALSE;
      }
    }

    if (isset($filename)) {
      if ($this->sl_checkpath[$context]) {
        if (file_exists($this->sl_paths[$context] . '/' . $filename)) {
          return $this->sl_paths[$context] . '/' . $filename;
        }
        else {
          return $this->sl_paths[$default] . '/' . $filename;
        }
      }
      else {
        return $this->sl_paths[$context] . '/' . $filename;
      }
    }
    else {
      return $this->sl_paths[$context];
    }
  }

  /**
   * Check whether the service links should be displayed.
   *
   * Check whether the service links should be displayed for the content type,
   * for category or one of the other selected options.
   *
   * @param \Drupal\Core\Entity\Entity $entity
   *   The node to check.
   *
   * @return bool
   *   Whether to show service links for the given entity.
   */
  public function entityDisplayIsEnabled(Entity $entity) {
    $links_show = TRUE;
    $user = $this->currentUser;

    $config = $this->configFactory->getEditable($this->settingsKey);
    $hide_for_author = $config->get('hide_for_author');
    if ($hide_for_author && ($user->id() == $entity->getRevisionUserId())) {
      return FALSE;
    }

    $hide_if_unpublished = $config->get('hide_if_unpublished');
    if ($hide_if_unpublished && !$entity->isPublished()) {
      return FALSE;
    }

    $this->moduleHandler->alter('service_links_entity_display_enabled', $links_show);

    return $links_show;
  }

  /**
   * Return the entity label to use.
   *
   * @param \Drupal\Core\Entity\Entity $entity
   *   The node instance.
   *
   * @return string
   *   The title text.
   */
  public function getEntityLabel(Entity $entity) {
    $config = $this->configFactory->getEditable($this->settingsKey);
    $title = $entity->label();

    switch ($config->get('override_title')) {
      case self::SERVICE_LINKS_TAG_TITLE_ENTITY:
        break;

      case self::SERVICE_LINKS_TAG_TITLE_OVERRIDE:
        $title = str_replace('<title>', $title, $config->get('override_title_text'));
        break;

      case self::SERVICE_LINKS_TAG_TITLE_TOKEN:
        $title = \Drupal::token()
          ->replace($config->get('override_title_text'), ['entity' => $entity]);
        break;
    }

    return $title;
  }

  /**
   * Split a query string into the proper array.
   *
   * @param string $query_str
   *   The query string to be broken into components.
   *
   * @return array|null
   *   The parts of the string.
   */
  public function getQuery($query_str) {
    if (empty($query_str)) {
      return NULL;
    }
    $result = [];

    // parse_str() replaces dots and squares with underscores.
    $query_str = explode('&', $query_str);
    foreach ($query_str as $q) {
      $key_value = explode('=', $q);
      $result[$key_value[0]] = isset($key_value[1]) ? $key_value[1] : '';
    }

    return $result;
  }

  /**
   * Create short links using predefined settings.
   *
   * @param string $url
   *   The URL to be shortened.
   * @param string|null $entity_type
   *   The entity type.
   * @param int|null $eid
   *   The node id.
   *
   * @return array
   *   The shortened URL and additional cache tags.
   */
  public function shortUrl($url, $entity_type = '', $eid = NULL) {
    $config = $this->configFactory->getEditable($this->settingsKey);
    $cache_tags = [];

    switch ($config->get('short_links_type')) {
      case self::SERVICE_LINKS_SHORT_URL_TYPE_ENTITY:
        if (empty($eid)) {
          break;
        }

        // With alias = true doesn't change the path.
        $url = Url::fromUri("entity:{$entity_type}/{$eid}",
          [
            'absolute' => TRUE,
            'alias' => TRUE,
          ]
        )->toString();
        break;

      case self::SERVICE_LINKS_SHORT_URL_TYPE_SERVICE:
        $cache_tags[] = 'config:core.extension';
        if ($this->moduleHandler->moduleExists('shorten')) {
          $turl = shorten_url($url);
        }
        else {
          try {
            $response = \Drupal::httpClient()->get(
              'http://tinyurl.com/api-create.php?url=' . urlencode($url),
              ['headers' => ['Accept' => 'text/plain']]
            );
            $turl = (string) $response->getBody();
            if (empty($turl)) {
              break;
            }
          }
          catch (RequestException $e) {
            break;
          }
        }

        $url = $turl;
        break;

      case self::SERVICE_LINKS_SHORT_URL_TYPE_REDIRECT_DOMAIN:
        $burl = $config->get('domain_redirect');

        $url = Url::fromUri("entity:{$entity_type}/{$eid}",
          ['absolute' => TRUE, 'base_url' => $burl]
        )->toString();
        break;

      case self::SERVICE_LINKS_SHORT_URL_TYPE_REDIRECT_ALL:
        $burl = $config->get('domain_redirect');
        if (empty($eid)) {
          $url = Url::fromRoute('entity.node.canonical',
            ['absolute' => TRUE, 'base_url' => $burl]
          )->toString();
        }
        else {
          $url = Url::fromUri("entity:{$entity_type}/{$eid}",
            [
              'absolute' => TRUE,
              'alias' => TRUE,
              'base_url' => $burl,
            ])->toString();
        }
        break;

      default:
        drupal_set_message('Unhandled ShortUrl type in ServiceLinksController');
        break;

    }

    return [$url, $cache_tags];
  }

  /**
   * Fill an array with tokens and the content to substitute.
   *
   * @param mixed $dataSource
   *   The entity instance or an array of fixed values.
   *
   * @returns array
   *   An array of tokens and substitutions.
   */
  public function getTokens($dataSource) {
    $config = $this->configFactory->getEditable($this->settingsKey);
    $result = [];
    $cache_tags = [];

    if (is_array($dataSource)) {
      $title = isset($dataSource['title']) ? $dataSource['title'] : '';
      $url = isset($dataSource['url']) ? $dataSource['url'] : '';
      $query = isset($dataSource['query']) ? $dataSource['query'] : '';
      $teaser = isset($dataSource['teaser']) ? $dataSource['teaser'] : '';
      $eid = isset($dataSource['node_id']) ? $dataSource['node_id'] : NULL;
      $entity_type = isset($dataSource['node_id']) ? 'node' : '';
    }
    elseif (!empty($dataSource)) {
      $title = $this->getEntityLabel($dataSource);
      $eid = $dataSource->id();
      $entity_type = $dataSource->getEntityTypeId();

      try {
        $url = Url::fromUri("entity:{$entity_type}/{$eid}", [
          'absolute' => TRUE,
          'query' => $this->getQuery(strip_tags($config->get('append_to_url')))
        ])->toString();
      }
      catch(RouteNotFoundException $exception) {
        return [ FALSE, []];
      }

      $query = \Drupal::service('path.alias_manager')
        ->getAliasByPath(Url::fromUri("entity:{$entity_type}/{$eid}",
          ['absolute' => FALSE])
          ->toString());

      if ($dataSource->hasField('body')) {
        $body_summary = $dataSource->get('body')->summary;
        $teaser = $body_summary ? strip_tags($body_summary) : '';
      }
      else {
        $teaser = '';
      }
    }
    else {
      $request = \Drupal::request();
      $route_match = \Drupal::routeMatch();
      $title = \Drupal::service('title_resolver')
        ->getTitle($request, $route_match->getRouteObject());
      $title = (string) $title;
      $is_front = \Drupal::service('path.matcher')->isFrontPage();

      $query = ServiceLinksController::getQuery(strip_tags($config->get('append_to_url')));
      if ($is_front) {
        $route = '<front>';
        $url = Url::fromRoute($route, [
          'absolute' => TRUE,
          'query' => $query,
        ])
          ->toString();
      }
      else {
        $baseUrl = \Drupal::request()->getSchemeAndHttpHost();
        $url = $baseUrl . $this->currentPathStack->getPath();
      }

      // The query shouldn't follow the front_page alias but point the original
      // page.
      $query = \Drupal::service('path.alias_manager')
        ->getPathByAlias($this->currentPathStack->getPath());

      $teaser = '';
      $eid = NULL;
      $entity_type = '';
    }

    $source = $this->configFactory->getEditable('system.site')
      ->get('name');

    $cache_tags[] = 'config:system.site';

    $long_url = $url;

    $front_page = Url::fromRoute('<front>', [], ['absolute' => TRUE])
      ->toString();

    switch ($config->get('short_links_use')) {
      case self::SERVICE_LINKS_SHORT_URL_USE_NEVER:
        $short_url = $url;
        break;

      case self::SERVICE_LINKS_SHORT_URL_USE_WHEN_REQUESTED:
        list($short_url, $additional_tags) = $this->shortUrl($url, $entity_type, $eid);
        $cache_tags = array_merge($cache_tags, $additional_tags);
        break;

      case self::SERVICE_LINKS_SHORT_URL_USE_ALWAYS:
        list($short_url, $additional_tags) = $this->shortUrl($url, $entity_type, $eid);
        $cache_tags = array_merge($cache_tags, $additional_tags);
        $url = $short_url;
        break;
    }

    $result['tag'] = [
      'raw-encoded-title' => '<raw-encoded-title>',
      'raw-encoded-url' => '<raw-encoded-url>',
      'raw-encoded-teaser' => '<raw-encoded-teaser>',
      'raw-encoded-short-url' => '<raw-encoded-short-url>',
      'raw-encoded-long-url' => '<raw-encoded-long-url>',
      'raw-encoded-query' => '<raw-encoded-query>',
      'raw-encoded-source' => '<raw-encoded-source>',
      'encoded-title' => '<encoded-title>',
      'encoded-url' => '<encoded-url>',
      'encoded-teaser' => '<encoded-teaser>',
      'encoded-short-url' => '<encoded-short-url>',
      'encoded-long-url' => '<encoded-long-url>',
      'encoded-query' => '<encoded-query>',
      'encoded-source' => '<encoded-source>',
      'teaser' => '<teaser>',
      'short-url' => '<short-url>',
      'long-url' => '<long-url>',
      'source' => '<source>',
      'entity-id' => '<entity-id>',
      'query' => '<query>',
      'url' => '<url>',
      'title' => '<title>',
      'front-page' => '<front-page>',
      'entity-type' => '<entity-type>',
    ];
    $result['subst'] = [
      'raw-encoded-title' => $title,
      'raw-encoded-url' => $url,
      'raw-encoded-teaser' => $teaser,
      'raw-encoded-short-url' => $short_url,
      'raw-encoded-long-url' => $long_url,
      'raw-encoded-query' => $query,
      'raw-encoded-source' => $source,
      'encoded-title' => Html::escape($title),
      'encoded-url' => Html::escape($url),
      'encoded-teaser' => Html::escape($teaser),
      'encoded-short-url' => Html::escape($short_url),
      'encoded-long-url' => Html::escape($long_url),
      'encoded-query' => Html::escape($query),
      'encoded-source' => Html::escape($source),
      'teaser' => $teaser,
      'short-url' => $short_url,
      'long-url' => $long_url,
      'source' => $source,
      'entity-id' => $eid,
      'query' => $query,
      'url' => $url,
      'title' => $title,
      'front-page' => $front_page,
      'entity-type' => $entity_type,
    ];
    // Invoke an alter hook to let other modules modify these values if needed.
    \Drupal::moduleHandler()
      ->alter('service_links_tokens', $result, $entity, $cache_tags);

    return [$result, $cache_tags];
  }

  /**
   * Get a list of service links.
   *
   * @param \Drupal\Core\Entity\Entity|null $entity
   *   The node being rendered (to use in access checks).
   * @param array $context
   *   The render context (also for access checks).
   *
   * @return array
   *   The list of service links to be rendered and cache tags for access.
   */
  public function getLinks(Entity $entity = NULL, array $context) {
    $access_cache_tags = [];

    $pluginManager = \Drupal::service('plugin.manager.service_link');
    $pluginDefinitions = $pluginManager->getDefinitions();

    $config = $this->configFactory->getEditable($this->settingsKey);

    $ordered = [];

    foreach ($pluginDefinitions as $plugin_id => $definition) {
      $plugin = $pluginManager->createInstance($plugin_id);
      $details = $plugin->getDetails();

      foreach ($details as $id => $detail) {
        $show = $config->get("show.${id}");
        $show = is_null($show) ? 0 : $show;

        if (!$show) {
          continue;
        }

        if (isset($detail['access'])) {
          $methodName = isset($detail['access']) ? $detail['access'] : '';
          if (!empty($methodName) && method_exists($plugin, $methodName)) {
            list($allowed, $tags) = $plugin->{$methodName}($entity, $context);
            $access_cache_tags = array_merge($access_cache_tags, $tags);
            if (!$allowed) {
              continue;
            }
          }
        }

        $weight = $config->get("weight.${id}");
        $weight = is_null($weight) ? 0 : $weight;

        if (!isset($ordered[$weight])) {
          $ordered[$weight] = [];
        }

        $item = [
          'id' => $id,
          'detail' => $detail,
          'show' => $show,
          'weight' => $weight,
          'service_id' => $plugin_id,
          'plugin_definition' => $definition,
          'plugin' => $plugin,
          'group' => isset($definition['name']) ? $definition['name'] : $definition['id'],
        ];

        $ordered[$weight][] = (object) $item;
      }
    }

    uksort($ordered, function ($a, $b) {
      if ($a == $b) {
        return 0;
      }

      return ($a < $b) ? -1 : 1;
    });

    $links = [];

    foreach ($ordered as $weight => $items) {
      $links = array_merge($links, $items);
    }

    return [$links, $access_cache_tags];
  }

  /**
   * The common render function used privately.
   *
   * @param string $service_id
   *   The unique ID for this service.
   * @param \stdClass $service
   *   Configuration details for the service.
   * @param \Drupal\Core\Entity\Entity|null $entity
   *   The entity to which links are being provided.
   * @param array $tokens
   *   An array of tokens and substitute values for the URL.
   */
  public function renderOne($service_id, $service, $entity = NULL, $tokens) {
    $config = $this->configFactory->getEditable($this->settingsKey);

    $methodName = isset($service->detail['preset']) ? $service->detail['preset'] : '';
    if (!empty($methodName) && method_exists($service->plugin, $methodName)) {
      list($service, $tokens) = $service->plugin->{$methodName}($service, $tokens, $entity);
    }

    // This tag should be filled before the split otherwise it will be lost.
    $service->detail['link'] = str_replace('<front-page>', $tokens['subst']['front-page'], $service->detail['link']);

    if (strpos($service->detail['link'], '=') === FALSE) {
      $service->url = [$service->detail['link']];
    }
    else {
      $service->url = preg_split('/\?/', $service->detail['link']);
    }

    if (count($service->url) > 1) {
      $service->url[1] = $this->getQuery($service->url[1]);
      $service->url[1] = str_replace($tokens['tag'], $tokens['subst'], $service->url[1]);
    }
    else {
      $service->url[0] = str_replace($tokens['tag'], $tokens['subst'], $service->url[0]);
    }

    $text = $service->detail['name'];

    $file = $this->expandPath(isset($service->icon) ? $service->icon : "$service_id.png");
    $icon = (file_exists($file)) ? '/' . $file : '';

    $methodName = isset($service->detail['callback']) ? $service->detail['callback'] : '';
    if (!empty($methodName) && method_exists($service->plugin, $methodName)) {
      $service = $service->plugin->{$methodName}($service, $tokens['subst']);
    }

    if (count($service->url) > 1) {
      $service->url[1] = http_build_query($service->url[1]);
      $service->url = implode('?', $service->url);
    }
    else {
      $service->url = $service->url[0];
    }

    if (isset($service->detail['internal']) && $service->detail['internal']) {
      $service->url = 'internal:/' . $service->url;
    }

    $service->url = Url::fromUri($service->url);

    $drupalSettings = isset($service->detail['drupalSettings']) ? $service->detail['drupalSettings'] : [];

    // Create the render array.
    $render_array = [
      '#type' => 'service_link',
      '#text' => $text,
      '#image' => [
        '#theme' => 'image',
        '#uri' => $icon,
        '#alt' => $text,
      ],
      '#url' => $service->url,
      '#style' => empty($service->style) ? $this->formatterSettings['link_style'] : $service->style,
      '#attributes' => [
        'class' => [
          str_replace([
            '][',
            '_',
            ' ',
          ], '-', 'service_links-' . $service_id),
        ],
        'rel' => 'nofollow',
        'title' => $service->detail['description'],
      ],
      '#attached' => [
        'library' => [],
        'drupalSettings' => $drupalSettings,
      ],
    ];

    if ($config->get('new_window')) {
      $render_array['#attributes']['target'] = '_blank';
    }

    // Add the related JavaScript and CSS.
    if (isset($service->detail['library'])) {
      $render_array['#attached']['library'][] = $service->detail['library'];
    }

    return [$service_id => $render_array];
  }

  /**
   * Function that builds render arrays for all the selected services.
   *
   * @param \Drupal\Core\Entity\Entity|null $entity
   *   Contains the content structured as a node object.
   *   Use the node_load() function to build one or set
   *   NULL this variable to consider the current page.
   * @param array $context
   *   The render context (if any) to use in access checks.
   *
   * @return array
   *   An array of render arrays for each link.
   */
  public function build(Entity $entity = NULL, array $context = []) {
    $links = ['link' => [], 'weight' => []];
    $cache_tags = [ 'user.permissions' ];

    if (!\Drupal::currentUser()->hasPermission('access service links')) {
      return [[], $cache_tags];
    }

    $cache_tags[] = 'config:' . $this->settingsKey;

    if ($entity) {
      $cache_tags[] = "{$entity->getEntityTypeId()}:{$entity->id()}";

      if (!$this->entityDisplayIsEnabled($entity)) {
        return [[], $cache_tags];
      }
    }

    list($tokens, $tags) = $this->getTokens($entity);
    if (!$tokens) {
      return [[], $cache_tags];
    }
    $cache_tags = array_merge($cache_tags, $tags);

    list($services, $tags) = $this->getLinks($entity, $context);
    $cache_tags = array_merge($cache_tags, $tags);

    foreach ($services as $service) {

      $links['weight'][] = $service->weight;

      // Render the Service.
      $links['link'] += $this->renderOne($service->id, $service, $entity, $tokens);
    }

    if (!empty($links['link'])) {
      array_multisort($links['weight'], $links['link']);
    }

    // We don't care about the actual weights. What is important here is what
    // links are shown and in what order. Tags are added in each link for the
    // link specific configuration.
    $cache_tags[] = 'service_link_order:' .
      implode(',', array_keys($links['link']));

    return [$links['link'], $cache_tags];
  }

  /**
   * @param \Drupal\Core\Entity\Entity $entity
   *   The entity to which links will point.
   * @param \Drupal\Core\Entity\EntityInterface $contextEntity
   *   The entity in which the links field is found.
   * @param string $fieldName
   *   The name of the field referring to the links configuration.
   *
   * @return array
   *   Render array.
   */
  public function fieldFormatterView(Entity $entity, Entity $contextEntity, $fieldName) {

    list($entity->service_links, $cache_tags) = $this->build($entity);

    return [
      '#type' => 'service_links',
      '#style' => $this->formatterSettings['theme_variant'],
      '#links' => $entity->service_links,
      '#entity_type' => $entity->getEntityTypeId(),
      '#container_type' => $contextEntity->getEntityTypeId(),
      '#container_field' => $fieldName,
      '#cache' => [
        'tags' => $cache_tags,
      ],
    ];

  }

}
