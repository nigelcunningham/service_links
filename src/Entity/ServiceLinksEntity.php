<?php

namespace Drupal\service_links\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\service_links\EntityInterface;

/**
 * Defines the Service Links entity.
 *
 * @ConfigEntityType(
 *   id = "service_links",
 *   label = @Translation("Set of Service Links"),
 *   label_singular = @Translation("Set of Service Links"),
 *   label_plural = @Translation("Sets of Service Links "),
 *   label_count = @PluralTranslation(
 *     singular = "@count set of Service Links",
 *     plural = "@count sets of Service Links",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\service_links\Controller\ServiceLinksEntityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\service_links\Form\ServiceLinksEntityForm",
 *       "edit" = "Drupal\service_links\Form\ServiceLinksEntityForm",
 *       "delete" = "Drupal\service_links\Form\ServiceLinksEntityDeleteForm",
 *     }
 *   },
 *   config_prefix = "service_links",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   links = {
 *     "edit-form" = "/admin/config/services/service_links/sets/{service_links}/edit",
 *     "delete-form" = "/admin/config/services/service_links/sets/{service_links}/delete",
 *   }
 * )
 */
class ServiceLinksEntity extends ConfigEntityBase {

  /**
   * The Service Links Entity ID.
   *
   * @var string
   */
  public $id;

  /**
   * The Service Links Entity label.
   *
   * @var string
   */
  public $label;

}
