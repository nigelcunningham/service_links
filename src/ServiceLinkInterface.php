<?php

namespace Drupal\service_links;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Service link plugins.
 */
interface ServiceLinkInterface extends PluginInspectionInterface {
}
