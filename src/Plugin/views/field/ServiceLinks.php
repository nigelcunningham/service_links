<?php

namespace Drupal\service_links\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\service_links\Controller\ServiceLinksController;
use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ServiceLinks.
 *
 * Field handler to provide a simple renderer for service links.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("service_links")
 */
class ServiceLinks extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);

    if (!empty($options['layout'])) {
      $this->additional_fields['layout'] = 'layout';
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['layout'] = ['default' => 'service_links'];
    return $options;
  }

  /**
   * Provide link to file option.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $form['layout'] = [
      '#title' => $this->t('Layout'),
      '#description' => $this->t("The format to use in displaying the service links."),
      '#type' => 'select',
      '#options' => [
        'Normal' => 'A simple list of links',
        'Fisheye' => 'Use a fisheye display',
      ],
      '#default_value' => !empty($this->options['layout']),
    ];
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $node = $values->_entity;

    $nodelink = !!$node;

    $controller = ServiceLinksController::create(\Drupal::getContainer());

    list($links, $cache_tags) = $controller->build($node, $nodelink);

    if ($this->options['layout'] == 'Fisheye') {
      $theme = 'service_links_fisheye';
    }
    else {
      $theme = 'service_links';
    }

    return [
      '#theme' => $theme,
      '#links' => $links,
    ];
  }

}
