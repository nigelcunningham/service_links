<?php

namespace Drupal\service_links\Plugin\Field\FieldFormatter;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\service_links\Controller\ServiceLinksController;

/**
 * Plugin implementation of the Service Links field formatter.
 *
 * @FieldFormatter(
 *   id = "service_links",
 *   label = @Translation("Service Links"),
 *   description = @Translation("Display service links using the chosen configuration."),
 *   field_types = {
 *     "entity_reference"
 *   },
 * )
 */
class ServiceLinksFieldFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return ($field_definition->getSetting('target_type') == 'service_links');
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {

    $options = [
      'theme_variant' => 'default',
      'link_style' => ServiceLinksController::SERVICE_LINKS_STYLE_IMAGE_AND_TEXT,
    ] + parent::defaultSettings();
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    switch ($this->getSetting('theme_variant')) {
      case 'fisheye':
        $summary[] = $this->t('Theme variant: Fisheye display');
        break;

      default:
        $summary[] = $this->t('Theme variant: List of links');
        break;

    }

    switch ($this->getSetting('link_style')) {
      case ServiceLinksController::SERVICE_LINKS_STYLE_TEXT:
        $summary[] = $this->t('Link style: Text only');
        break;

      case ServiceLinksController::SERVICE_LINKS_STYLE_IMAGE:
        $summary[] = $this->t('Link style: Image only');
        break;

      case ServiceLinksController::SERVICE_LINKS_STYLE_IMAGE_AND_TEXT:
        $summary[] = $this->t('Link style: Image and text');
        break;

      case ServiceLinksController::SERVICE_LINKS_STYLE_EMPTY:
        $summary[] = $this->t('Link style: Empty (Javascript provided)');
        break;

      case ServiceLinksController::SERVICE_LINKS_STYLE_FISHEYE:
        $summary[] = $this->t('Link style: Fisheye optimised');
        break;

      default:
        $summary[] = $this->t('Link style: Unrecognised value');
        break;

    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements['theme_variant'] = [
      '#title' => t('Wrapper variant'),
      '#description' => t('The style to use for the wrapper containing links'),
      '#type' => 'select',
      '#options' => [
        'default' => $this->t('List of links'),
        'fisheye' => $this->t('Fisheye'),
      ],
      '#default_value' => $this->getSetting('theme_variant'),
      '#cache' => [
        '#tags' => [
          'service_links_formatter_settings',
        ]
      ]
    ];

    $elements['link_style'] = [
      '#title' => t('Link display variant'),
      '#description' => t('The style to use for the display of individual links'),
      '#type' => 'select',
      '#options' => [
        ServiceLinksController::SERVICE_LINKS_STYLE_TEXT => $this->t('Text only'),
        ServiceLinksController::SERVICE_LINKS_STYLE_IMAGE => $this->t('Image only'),
        ServiceLinksController::SERVICE_LINKS_STYLE_IMAGE_AND_TEXT=> $this->t('Image and text'),
        ServiceLinksController::SERVICE_LINKS_STYLE_EMPTY=> $this->t('Empty (Javascript provided)'),
        ServiceLinksController::SERVICE_LINKS_STYLE_FISHEYE=> $this->t('Fisheye optimised'),
      ],
      '#default_value' => $this->getSetting('link_style'),
      '#cache' => [
        '#tags' => [
          'service_links_formatter_settings',
        ]
      ]
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $element = [];

    // What is the entity for the page as a whole (the entity to which links
    // should point).
    $contextEntity = $entity = $items->getEntity();

    foreach (\Drupal::routeMatch()->getParameters() as $param) {
      if ($param instanceof \Drupal\Core\Entity\EntityInterface &&
        ($param->getEntityTypeId() !== 'view')) {
        $entity = $param;
        break;
      }
    }

    $formatterSettings = $this->getSettings();

    foreach ($items as $delta => $item) {
      $serviceLinksEntity = $item->get('entity')->getTarget()->getValue();
      $settingsKey = $settingsKey = is_null($serviceLinksEntity->id()) ? 'service_links.settings' : 'service_links.entity.' . $serviceLinksEntity->id();

      // Get render arrays for each value.
      $controller = ServiceLinksController::create(\Drupal::getContainer(), $settingsKey, $formatterSettings);
      $element[$delta] = $controller->fieldFormatterView($entity, $contextEntity, $items->getName());
    }

    $element['#cache']['tags'][] = 'service_links_formatter_settings';
    return $element;
  }

}
