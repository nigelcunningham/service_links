<?php

namespace Drupal\service_links\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Container;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for the ServiceLinksEntity add and edit forms.
 */
class ServiceLinksEntityForm extends EntityForm {

  /**
   * Save the container and use for getting settings.
   *
   * @var ContainerInterface
   */
  protected $container;

  /**
   * Constructs an ServiceLinksEntityForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entityTypeManager.
   * @param ContainerInterface $container
   *   The container interface, reused in getting the settings form.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, ContainerInterface $container) {
    $this->entityTypeManager = $entityTypeManager;
    $this->container = $container;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $service_links = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Set of Service Links'),
      '#maxlength' => 255,
      '#default_value' => $service_links->label(),
      '#description' => $this->t("Name for this set of Service Links."),
      '#required' => TRUE,
      '#weight' => -50,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $service_links->id(),
      '#machine_name' => [
        'exists' => [$this, 'exist'],
      ],
      '#disabled' => !$service_links->isNew(),
      '#weight' => -49,
    ];


    // Add the bulk of the form in ServiceLinksConfigurationForm class.
    // If no entity exists yet, use the sitewide form to provide defaults.
    // I deliberately avoid using the form API because it's not designed for
    // embedding one form in another. (I tried but it was too messy).
    $settingsKey = is_null($service_links->id()) ? 'service_links.settings' : 'service_links.entity.' . $service_links->id();

    ServiceLinksConfigurationForm::addFormElements($form, $form_state, $settingsKey, FALSE);

    // Invoke hook_form_alter implementations.
    $this->moduleHandler->alter('form_service_links_configuration', $form, $form_state, $form_id);

    $form['actions']['submit']['#submit'] = $form['#submit'];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $service_links = $this->entity;
    $status = $service_links->save();

    // Invoke the other submit handlers. I would have thought this would just
    // work, like it does for validation. Not so much, though. Hence...
    foreach ($form['#submit'] as $callback) {
      if ($callback == '::submitForm') {
        continue;
      }

      call_user_func_array($form_state->prepareCallback($callback), [&$form, &$form_state]);
    }

    if ($status) {
      drupal_set_message($this->t('Saved the %label set of Service Links.', [
        '%label' => $service_links->label(),
      ]));
    }
    else {
      drupal_set_message($this->t('The %label set of Service Links was not saved.', [
        '%label' => $service_links->label(),
      ]));
    }

    // Invoke the submit handlers for the 'body' we added.

    $form_state->setRedirect('entity.service_links.collection');
  }

  /**
   * Helper function to check whether an Service Links configuration entity
   * exists.
   */
  public function exist($id) {
    $entity = $this->entityTypeManager->getStorage('service_links')->getQuery()
      ->condition('id', $id)
      ->execute();
    return (bool) $entity;
  }

}