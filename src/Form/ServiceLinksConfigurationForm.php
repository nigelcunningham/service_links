<?php

namespace Drupal\service_links\Form;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Entity\EntityManager;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\service_links\Controller\ServiceLinksController;
use Drupal\Core\Block\BlockManager;
use Drupal\service_links\ServiceLinkManager;

/**
 * Provides the sitewide settings form and the content for
 * instances of the entity form.
 */
class ServiceLinksConfigurationForm extends ConfigFormBase {

  /**
   * Module Handler Service.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * Config Factory Service.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Block Manager Service.
   *
   * @var \Drupal\Core\Block\BlockManager
   */
  protected $blockPluginManager;

  /**
   * Current User Service.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Entity Manager Service.
   *
   * @var \Drupal\Core\Entity\EntityManager
   */
  protected $entityManager;

  /**
   * Entity Query Service.
   *
   * @var \Drupal\Core\Entity\Query\QueryFactory
   */
  protected $entityQuery;

  /**
   * Service Links Controller.
   *
   * @var \Drupal\service_links\Controller
   */
  protected $serviceLinksController;

  /**
   * Service Link Manager instance.
   *
   * @var serviceLinkManager
   */
  private $serviceLinkManager;

  /**
   * The settings key being used to provide default values.
   *
   * @var string
   */
  protected $settingsKey;

  /**
   * Whether we're building / processing the sitewide form.
   *
   * @var bool
   */
  protected $isSitewideForm;

  /**
   * Constructs a new form object.
   *
   * @param \Drupal\Core\Extension\ModuleHandler $moduleHandler
   *   The module handler service.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The configuration factory service.
   * @param \Drupal\Core\Block\BlockManager $blockPluginManager
   *   The block plugin manager service.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user service.
   * @param \Drupal\Core\Entity\EntityManager $entityManager
   *   The entity manager service.
   * @param \Drupal\Core\Entity\Query\QueryFactory $entityQuery
   *   The entity query service.
   * @param \Drupal\service_links\Controller\ServiceLinksController $serviceLinksController
   *   The service links controller.
   * @param \Drupal\service_links\ServiceLinkManager $serviceLinkManager
   *   The service link manager.
   * @param string $settings_key
   *   The settings key being used to provide default values.
   * @param bool $isSitewide
   *   Whether the sitewide settings form is being displayed.
   */
  public function __construct(ModuleHandler $moduleHandler, ConfigFactory $configFactory, BlockManager $blockPluginManager, AccountInterface $currentUser, EntityManager $entityManager, QueryFactory $entityQuery, ServiceLinksController $serviceLinksController, ServiceLinkManager $serviceLinkManager) {
    $this->moduleHandler = $moduleHandler;
    $this->configFactory = $configFactory;
    $this->blockPluginManager = $blockPluginManager;
    $this->currentUser = $currentUser;
    $this->entityManager = $entityManager;
    $this->entityQuery = $entityQuery;
    $this->serviceLinksController = $serviceLinksController;
    $this->serviceLinkManager = $serviceLinkManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
      $container->get('config.factory'),
      $container->get('plugin.manager.block'),
      $container->get('current_user'),
      $container->get('entity.manager'),
      $container->get('entity.query'),
      $container->get('plugin.manager.service_links_controller'),
      $container->get('plugin.manager.service_link')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'service_links_configuration';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [$this->settingsKey];
  }

  /**
   * Add elements to a form - either the sitewide form (below) or an entity form.
   *
   * @param array $form
   *   The form to which elements should be added.
   * @param FormStateInterface $form_state
   *   The (unused) form state.
   * @param string $settingsKey
   *   The config key to use in getting default values.
   * @param bool $isSitewide
   *   Whether the sitewide form is being built.
   */
  public static function addFormElements(array &$form, FormStateInterface $form_state, $settingsKey = 'service_links.settings', $isSitewide = TRUE) {

    $config = \Drupal::configFactory()->getEditable($settingsKey);
    $serviceLinksController = \Drupal::getContainer()->get('plugin.manager.service_links_controller');

    // Provide context info to modules altering this form, so the normal
    // form_alter hook can be used.
    $form['#sitewide'] = $isSitewide;
    $form['#settingsKey'] = $settingsKey;

    $form['how_to_show_the_links'] = [
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#title' => t('How to display Service Links'),
    ];

    $form['how_to_show_the_links']['style'] = [
      '#type' => 'select',
      '#title' => t('Choose a style'),
      '#default_value' => $config->get('style'),
      '#options' => [
        ServiceLinksController::SERVICE_LINKS_STYLE_TEXT => t('Only Text'),
        ServiceLinksController::SERVICE_LINKS_STYLE_IMAGE => t('Only Image'),
        ServiceLinksController::SERVICE_LINKS_STYLE_IMAGE_AND_TEXT => t('Image and Text'),
      ],
    ];

    $styles = \Drupal::moduleHandler()->invokeAll('sl_styles', [FALSE]);
    if (!empty($styles)) {
      $form['how_to_show_the_links']['style']['#options'] += $styles;
    }

    $form['how_to_show_the_links']['hide_if_unpublished'] = [
      '#type' => 'checkbox',
      '#title' => t("Don't show links if the content is unpublished"),
      '#default_value' => $config->get('hide_if_unpublished'),
    ];

    $form['how_to_show_the_links']['hide_for_author'] = [
      '#type' => 'checkbox',
      '#title' => t("Don't show links if the actual user is the author of the entity"),
      '#default_value' => $config->get('hide_for_author'),
    ];

    $form['how_to_show_the_links']['icons'] = [
      '#type' => 'fieldset',
      '#title' => t('Service Links icons'),
    ];

    $form['how_to_show_the_links']['icons']['path_icons'] = [
      '#type' => 'textfield',
      '#title' => t('Standard folder'),
      '#description' => t('If you have alternative icons enter here the relative path from your index.php without trailing slash (i.e. %path1 or %path2)', [
        '%path1' => 'files/newicons',
        '%path2' => 'sites/all/files/newicons',
      ]),
      '#default_value' => $serviceLinksController->expandPath(NULL, 'icons'),
      '#size' => 40,
    ];

    $form['how_to_show_the_links']['icons']['check_icons'] = [
      '#type' => 'checkbox',
      '#title' => t('Use the default icons if missing'),
      '#description' => t('Check every icon in the standard folder and if not available, consider the default path %path', ['%path' => $serviceLinksController->expandPath(NULL, 'preset')]),
      '#default_value' => $config->get('check_icons'),
    ];

    $form['extra_options'] = [
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#title' => t('Extra Options'),
    ];

    $form['extra_options']['new_window'] = [
      '#type' => 'select',
      '#title' => t('Open link in...'),
      '#default_value' => $config->get('new_window'),
      '#options' => [0 => t('Same window'), 1 => t('New window')],
    ];

    $form['extra_options']['append_to_url'] = [
      '#type' => 'textfield',
      '#title' => t('Append the following text to your URL'),
      '#description' => t('This text will be inserted at the end of the current URL (i.e. %param => %url)', [
        '%param' => 'param1=value1&param2=value2',
        '%url' => 'http://domain.com/current-page&param1=value1&param2=value2',
      ]),
      '#default_value' => $config->get('append_to_url'),
      '#size' => 40,
    ];

    $form['extra_options']['title'] = [
      '#type' => 'fieldset',
    ];

    $form['extra_options']['title']['override_title'] = [
      '#type' => 'select',
      '#title' => t('How to fill the title tag'),
      '#default_value' => $config->get('override_title'),
      '#options' => [
        ServiceLinksController::SERVICE_LINKS_TAG_TITLE_ENTITY => t('Use the original entity title'),
        ServiceLinksController::SERVICE_LINKS_TAG_TITLE_OVERRIDE => t('Override the original title'),
      ],
    ];

    $form['extra_options']['title']['override_title_text'] = [
      '#type' => 'textfield',
      '#description' => t("Enter the text that override the title: use @title for referrer the original title, either the various tokens offered by <a href='@url'>Token</a> if it's enabled and the related option active.", [
        '@url' => 'http://www.drupal.org/project/token',
        '@title' => '<title>',
      ]),
      '#default_value' => $config->get('override_title_text'),
      '#size' => 40,
    ];

    if (\Drupal::moduleHandler()->moduleExists('token')) {
      $form['extra_options']['title']['override_title']['#options'] += [
        ServiceLinksController::SERVICE_LINKS_TAG_TITLE_TOKEN => t('Parse the string with Token'),
      ];
      $form['extra_options']['title']['token_help'] = [
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#title' => t('Token help'),
        '#theme' => 'token_tree_link',
        '#token_types' => ['entity'],
      ];
    }

    $form['short_links'] = [
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#title' => t('Short Links'),
    ];

    $default = $config->get('short_links_use');
    $form['short_links']['short_links_use'] = [
      '#type' => 'select',
      '#title' => t('Use short links'),
      '#default_value' => $default,
      '#options' => [
        ServiceLinksController::SERVICE_LINKS_SHORT_URL_USE_NEVER => t('Never'),
        ServiceLinksController::SERVICE_LINKS_SHORT_URL_USE_WHEN_REQUESTED => t('Only when requested'),
        ServiceLinksController::SERVICE_LINKS_SHORT_URL_USE_ALWAYS => t('Always'),
      ],
    ];

    if ($default > 0) {
      $form['short_links']['short_links_type'] = [
        '#type' => 'select',
        '#title' => t('How generate short links'),
        '#default_value' => $config->get('short_links_type'),
        '#description' => t('If you select "Short URL," it will use the service selected with the <a href="@shorten">Shorten URLs module</a>. If you have not enabled the module, Service Links will default to TinyURL.', ['@shorten' => 'http://drupal.org/project/shorten']),
        '#options' => [
          ServiceLinksController::SERVICE_LINKS_SHORT_URL_TYPE_ENTITY => t('Use entity/xxx alias'),
          ServiceLinksController::SERVICE_LINKS_SHORT_URL_TYPE_SERVICE => t('Use Short Url service'),
          ServiceLinksController::SERVICE_LINKS_SHORT_URL_TYPE_REDIRECT_DOMAIN => t('Redirect only the Domain name'),
          ServiceLinksController::SERVICE_LINKS_SHORT_URL_TYPE_REDIRECT_ALL => t('Combo: domain redirect and entity/xxx alias'),
        ],
      ];

      $form['short_links']['domain_redirect'] = [
        '#type' => 'textfield',
        '#title' => t('Domain to redirect'),
        '#description' => t('Enter the complete address without trailing slash (i.e. %name)', ['%name' => 'http://www.example.com']),
        '#default_value' => $config->get('domain_redirect'),
        '#size' => 40,
      ];
    }

    $services = \Drupal::getContainer()->get('plugin.manager.service_link')->getDefinitions();

    $form['service_links'] = [
      '#type' => 'table',
      '#header' => [
        t('Service'),
        t('Group'),
        t('Show'),
        t('Weight'),
      ],
      '#empty' => t('You need to load at least one Services module, please enable them in the <a href="@url">admin > modules</a> page',
        ['@url' => \Drupal::getContainer()->get('url_generator')->generateFromRoute('system.modules_list')]),
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'service-links-order-weight',
        ],
      ],
    ];

    // Sort the data by weight first.
    $ordered = [];

    foreach ($services as $service_id => $service) {
      $plugin = \Drupal::getContainer()->get('plugin.manager.service_link')->createInstance($service_id);
      $details = $plugin->getDetails();
      $pluginDefinition = $plugin->getPluginDefinition();

      foreach ($details as $id => $detail) {
        $show = $config->get("show.${id}");
        $show = is_null($show) ? 0 : $show;

        $weight = $config->get("weight.${id}");
        $weight = is_null($weight) ? 0 : $weight;

        if (!isset($ordered[$weight])) {
          $ordered[$weight] = [];
        }

        $item = [
          'id' => $id,
          'detail' => $detail,
          'show' => $show,
          'service_id' => $service_id,
          'service' => $service,
          'group' => isset($pluginDefinition['name']) ? $pluginDefinition['name'] : $pluginDefinition['id'],
        ];

        $ordered[$weight][] = (object) $item;
      }
    }

    uksort($ordered, function ($a, $b) {
      if ($a == $b) {
        return 0;
      }

      return ($a < $b) ? -1 : 1;
    });

    foreach ($ordered as $weight => $items) {
      foreach ($items as $item) {

        $form['service_links'][$item->id] = [
          '#attributes' => [
            'class' => [
              'draggable',
            ],
          ],
          '#weight' => $weight,

          'service' => [
            '#type' => 'service_label',
            '#label' => $item->detail['name'] ? $item->detail['name'] : ucwords(str_replace('_', ' ', $service_id)),
            '#icon' => isset($service['icon']) ? $serviceLinksController->expandPath($service['icon'], 'preset') : $serviceLinksController->expandPath("{$item->id}.png", 'preset'),
          ],

          'group' => [
            '#type' => 'plain_text',
            '#plain_text' => $item->group,
          ],

          'show' => [
            '#type' => 'checkbox',
            '#default_value' => $item->show,
          ],

          'weight' => [
            '#type' => 'weight',
            '#title' => t('Weight for @title', ['@title' => $service_id]),
            '#title_display' => 'invisible',
            '#default_value' => $weight,
            '#delta' => 50,
            '#attributes' => [
              'class' => ['service-links-order-weight'],
            ],
          ],
        ];
      }
    }

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Save changes'),
      // TableSelect: Enable the built-in form validation for #tableselect for
      // this form button, so as to ensure that the bulk operations form cannot
      // be submitted without any selected items.
      '#tableselect' => TRUE,
    ];

    $form['#validate'][] = [ '\Drupal\service_links\Form\ServiceLinksConfigurationForm', 'staticValidate'];
    $form['#submit'][] = [ '\Drupal\service_links\Form\ServiceLinksConfigurationForm', 'staticSubmit'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $settingsKey = 'service_links.settings', $isSitewide = TRUE) {
    self::addFormElements($form, $form_state, $settingsKey, $isSitewide);
    return parent::buildForm($form, $form_state);
  }

  /**
   * Validate the form submission.
   *
   * Check whether the domain address textbox is empty when should be not,
   * copy the selected terms into a unique list, check the path of custom'icons.
   *
   * {@inheritdoc}
   */
  public static function staticValidate(array &$form, FormStateInterface $form_state) {

    if (!is_null($form_state->getValue('short_links_type')) && (int) $form_state->getValue('short_links_type') > 2) {
      if (!$form_state->getValue('domain_redirect')) {
        $form_state->setError($form['short_links']['domain_redirect'], t('Domain redirect address is not set.'));
      }
      if (preg_match("/\/$/", $form_state->getValue('domain_redirect'))) {
        $form_state->setError($form['short_links']['domain_redirect'], t('No trailing slash!'));
      }
    }

    if (!empty($form_state->getValue('path_icons'))) {
      if (!is_dir($form_state->getValue('path_icons'))) {
        $form_state->setError($form['how_to_show_the_links']['icons']['path_icons'], t('The path for custom icons is not valid'));
      }
      elseif (preg_match("/\/$/", $form_state->getValue('path_icons'))) {
        $form_state->setError($form['how_to_show_the_links']['icons']['path_icons'], t('No trailing slash!'));
      }
    }
  }

  /**
   * Validate the form submission.
   *
   * Check whether the domain address textbox is empty when should be not,
   * copy the selected terms into a unique list, check the path of custom'icons.
   *
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    self::staticValidate($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public static function staticSubmit(array &$form, FormStateInterface $form_state) {
    $settingsKey = $form['#settingsKey'];

    if (is_null($settingsKey)) {
      $settingsKey = 'service_links';
    }

    $config = \Drupal::configFactory()->getEditable($settingsKey)
      ->set('open_target_blank', $form_state->getValue('open_target_blank'))
      ->set('css_include', $form_state->getValue('css_include'))
      ->set('style', $form_state->getValue('style'))
      ->set('hide_if_unpublished', $form_state->getValue('hide_if_unpublished'))
      ->set('hide_for_author', $form_state->getValue('hide_for_author'))
      ->set('path_icons', $form_state->getValue('path_icons'))
      ->set('check_icons', $form_state->getValue('check_icons'))
      ->set('new_window', $form_state->getValue('new_window'))
      ->set('append_to_url', $form_state->getValue('append_to_url'))
      ->set('override_title', $form_state->getValue('override_title'))
      ->set('override_title_text', $form_state->getValue('override_title_text'))
      ->set('short_links_use', $form_state->getValue('short_links_use'))
      ->set('short_links_type', $form_state->getValue('short_links_type'))
      ->set('domain_redirect', $form_state->getValue('domain_redirect'));

    foreach ($form['service_links'] as $key => $detail) {

      if (substr($key, 0, 1) == '#') {
        continue;
      }

      foreach (['show', 'weight'] as $attrib) {
        $search = ['service_links', $key, $attrib];
        $config_key = implode('.', [$attrib, $key]);
        $config->set($config_key, $form_state->getValue($search));
      }
    }

    $config->save();

    // Invalidate the block cache to update custom block-based derivatives.
    \Drupal::service('plugin.manager.block')->clearCachedDefinitions();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    self::staticSubmit($form, $form_state);

    parent::submitForm($form, $form_state);
  }
}
