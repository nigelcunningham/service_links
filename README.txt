Service Links 2.x:
------------------
Author and mantainer: Fabio Mucciante aka TheCrow (since the 2.x branch)
Current co-mantainer: Simon Georges
Port to Drupal 8 by:  Grindstone Creative (developer Nigel Cunningham)
Requirements:         Drupal 8
License:              GPL (see LICENSE.txt)

Introduction
------------
This module is the enhanced version of Service Links 1.x developed
by Fredrik Jonsson, rewritten and improved to fit the new purposes:
extend easily the number of services supported and provide APIs to
print links everywhere within any content, and then improved again
as part of the Drupal 8 port to support fields.
At the address http://servicelinks.altervista.org/?q=service
a web interface helps to create a module implementing services not
available in the standard package.

Overview
---------
Service Links provides support for quickly bookmarking pages using
social networks from around the world. Out of the box, it supports:

* del.icio.us
* Digg
* Facebook
* Google
* LinkedIn
* MySpace
* Reddit
* StumbleUpon
* Technorati
* Twitter
* Yahoo
* ...

The admin decides:
- the style to render the links: text, image, text + image
- where to show links
- what roles are allowed to see the selected links.

The 2.x branch introduced:
- modular management of services, grouped by different language area,
  through external modules implementing the hook_service_links()
- sorting of services through drag'n drop
- support for buttons which make use of Javascript without break the
  XHTML specifies to keep the module more 'accessible' as possible
- improved the use with not node pages
- support for other Drupal modules: Display Suite, Forward, Views, Short Url
- support for sprites to render the service images
- support for browser bookmarking (Chrome, Firefox, IE, Opera)
- two APIs to print easily the whole set of services or a customs subset of them
- configurable list of pages to show/hide on also through PHP code

The Drupal 8 port extends this by making service links a multivalue field linked
to a Configuration entity. This means it's now much easier to create subsets of
links and then display them anywhere fields can be added.

Thanks go to Lee Rowlands for some suggestions early in the process.

A more detailed list of options and related explanation is available at the page:
http://servicelinks.altervista.org/?q=about

Installation and configuration
-------------------------------
1) Copy the whole 'service_links' folder under your 'modules' directory and then
   
2) Point your browser to administer >> modules', enable 'Service Links' and one
   or more of the 'XXX Services' provided. 'General Services' contain the most
   commonly used social networks, and 'Widgets Services' the most used buttons.

3) Go to 'People >> Permissions' to allow users to use the links.

4) At 'Administration >> Configuration >> Web Services >> Service Links',
   set sitewide defaults for the display of links on the Sitewide defaults tab,
   defaults for which services are enabled on the Services tab and then
   create groups of links on the Configurations tab.

5) Modify node, taxonomy, user and other entities as desired, adding a Service
   Links reference field and choosing default Configurations to display.

More information
----------------

The file 'service_links.api.php' contains info about the hooks implemented

More info regarding installation and first configuration, set up of the
available options, either extension of the number of services and theming
output are available on the online documentation at the address:
http://servicelinks.altervista.org/?q=about

More services can be included and packed within an external module customizable
through a web interface available at the address:
http://servicelinks.altervista.org/?q=service