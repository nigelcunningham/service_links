<?php

namespace Drupal\Tests\service_links\Unit;

require_once __DIR__ . '/ServiceLinksControllerMockFunctions.php';

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\service_links\Controller\ServiceLinksController;
use Drupal\Tests\UnitTestCase;

/**
 * Tests for the ServiceLinksController.
 *
 * @group service_links
 */
class ServiceLinksControllerTest extends UnitTestCase {

  /**
   * The Service Links Controller instance.
   *
   * @var \Drupal\service_links\Controller\ServiceLinksController
   */
  protected $serviceLinksController;

  /**
   * Default configuration.
   *
   * @var array
   */
  protected $defaultConfig = [
    'path_icons' => NULL,
    'path_widgets' => '/widgets',
    'path_check1' => '/modules/custom/service_links/tests/src/Unit/assets',
    'check_check1' => TRUE,
    'short_links_use' => ServiceLinksController::SERVICE_LINKS_SHORT_URL_USE_NEVER,
    'short_links_type' => ServiceLinksController::SERVICE_LINKS_SHORT_URL_TYPE_SERVICE,
    'new_window' => FALSE,
    'style' => ServiceLinksController::SERVICE_LINKS_STYLE_IMAGE_AND_TEXT,
    'weight' => [],
    'show' => [],
    'append_to_url' => 'appended=true',
    'override_title' => ServiceLinksController::SERVICE_LINKS_TAG_TITLE_ENTITY,
    'override_title_text' => 'Replacement Title',
  ];

  /**
   * Non configuration key / value pairs used in configuring the test.
   *
   * @var array
   */
  protected $otherConfig;

  /**
   * Get mock module handler.
   *
   * @return \PHPUnit_Framework_MockObject_MockObject
   *   Mock module handler, configured as required for the test being run.
   */
  private function getMockModuleHandler() {

    $modulesExisting = [['taxonomy', TRUE]];

    switch ($this->getName()) {
      case 'shortUrlUsesShortenModuleIfAvailableAndSelected':
      case 'getTokensAppliesShortUrlWhenRequestedSettingWithNid':
        $modulesExisting[] = ['shorten', TRUE];
    }

    // Module / path to.
    $mockServiceLinksModule = $this->getMockBuilder('Drupal\Core\Extension\Extension')
      ->disableOriginalConstructor()
      ->getMock();

    $mockServiceLinksModule->method('getPath')
      ->willReturn('modules/custom/service_links');

    // Module handler - returns fake module instance.
    $moduleHandler = $this->createMock('Drupal\Core\Extension\ModuleHandler');
    $moduleHandler->method('getModule')
      ->with('service_links')
      ->willReturn($mockServiceLinksModule);
    $moduleHandler->method('moduleExists')
      ->willReturnMap($modulesExisting);

    return $moduleHandler;
  }

  /**
   * Create the mock config factory.
   *
   * @return \PHPUnit_Framework_MockObject_MockObject
   *   Mock config factory.
   */
  private function getMockConfigFactory() {
    $configMap = $this->defaultConfig;

    switch ($this->getName()) {

      case 'entityDisplayNotHiddenIfHiddenForAuthorAndNoAuthorMatch':
      case 'entityDisplayIsHiddenIfHiddenForAuthorAndAuthorMatches':
        $configMap['hide_for_author'] = TRUE;
        break;

      case 'entityDisplayIsHiddenIfUnpublishedAndHideIfUnpublished':
      case 'entityDisplayIsHiddenDoesNotHidePublishedEntitysWithHideIfUnpublished':
        $configMap['hide_if_unpublished'] = TRUE;
        break;

      case 'getEntityLabelReturnsOverridenTitleIfConfiguredToDoSo':
        $configMap['override_title'] = ServiceLinksController::SERVICE_LINKS_TAG_TITLE_OVERRIDE;
        break;

      case 'getEntityLabelReturnsTokenisedTitleIfConfiguredToDoSo':
        $configMap['override_title'] = ServiceLinksController::SERVICE_LINKS_TAG_TITLE_TOKEN;
        break;

      case 'shortUrlReturnsOriginalUrlForNonEntityUsingShortUrlTypeEntity':
      case 'shortUrlReturnsAbsoluteAliasedUsingShortUrlTypeEntity':
        $configMap['short_links_type'] = ServiceLinksController::SERVICE_LINKS_SHORT_URL_TYPE_ENTITY;
        break;

      case 'shortUrlUsesShortenModuleIfAvailableAndSelected':
      case 'shortUrlFallsBackToTinyUrlIfShortenUnavailable':
        $configMap['short_links_type'] = ServiceLinksController::SERVICE_LINKS_SHORT_URL_TYPE_SERVICE;
        break;

      case 'shortUrlCanReplaceBaseDomain':
        $configMap['short_links_type'] = ServiceLinksController::SERVICE_LINKS_SHORT_URL_TYPE_REDIRECT_DOMAIN;
        $configMap['domain_redirect'] = 'https://newdomain.net';
        break;

      case 'redirectAllReplacesBaseDomainWithoutEntity':
        $configMap['short_links_type'] = ServiceLinksController::SERVICE_LINKS_SHORT_URL_TYPE_REDIRECT_ALL;
        $configMap['domain_redirect'] = 'https://newdomain.net';
        break;

      case 'redirectAllReplacesBaseDomainWithEntity':
        $configMap['short_links_type'] = ServiceLinksController::SERVICE_LINKS_SHORT_URL_TYPE_REDIRECT_ALL;
        $configMap['domain_redirect'] = 'https://newdomain.net';
        break;

      case 'getTokensAppliesShortUrlWhenRequestedSettingWithNoNid':
      case 'getTokensAppliesShortUrlWhenRequestedSettingWithNid':
        $configMap['short_links_use'] = ServiceLinksController::SERVICE_LINKS_SHORT_URL_USE_WHEN_REQUESTED;
        break;

      case 'getTokensAlwaysAppliesShortUrlWhenRequestedWithNoNid':
      case 'getTokensAlwaysAppliesShortUrlWhenRequestedWithNid':
        $configMap['short_links_use'] = ServiceLinksController::SERVICE_LINKS_SHORT_URL_USE_ALWAYS;
        break;

      case 'buildDoesntRenderEntityIfDisplayDisabled':
        $configMap['hide_if_unpublished'] = TRUE;

      case 'renderOneAddsTargetBlankIfNewWindowSet':
        $configMap['new_window'] = TRUE;

      case 'getLinksOrdersPluginsByWeight':
      case 'getLinksPrefersPluginDefinitionNameOverId':
      case 'getLinksReturnsLinksForEnabledPlugins':
      case 'getLinksDoesntReturnPluginsWhereAccessDenied':
      case 'getLinksReturnsCacheTagsFromAccessChecks':
      case 'renderOneProducesExpectedRenderArray':
      case 'buildReturnsArrayOfLinksAndCacheTags':
      case 'buildDoesntRenderEntityIfNoPermission':
      case 'fieldFormatterViewReturnsTheExpectedRenderArray':
        $configMap['show.first'] = TRUE;
        $configMap['show.second_1'] = TRUE;
        $configMap['show.second_2'] = TRUE;
        $configMap['show.second_3'] = TRUE;
        $configMap['show.third'] = FALSE;
        $configMap['weight.first'] = 10;
        $configMap['weight.second_1'] = 20;
        $configMap['weight.second_2'] = 19;
        $configMap['weight.second_3'] = 19;
        $configMap['weight.third'] = 0;
        break;

      default:
        break;
    }

    // Translate to the format willReturnMap wants.
    $map = [];
    foreach ($configMap as $key => $value) {
      $map[] = [$key, $value];
    }

    $config = $this->getMockBuilder('Drupal\Core\Config\ImmutableConfig')
      ->disableOriginalConstructor()
      ->getMock();
    $config->expects($this->any())->method('get')
      ->willReturnMap($map);

    $siteConfigMap = [
      ['page.front', $this->otherConfig['front_page']],
      ['name', 'Site Name'],
    ];

    $siteConfig = $this->getMockBuilder('Drupal\Core\Config\ImmutableConfig')
      ->disableOriginalConstructor()
      ->getMock();
    $siteConfig->expects($this->any())->method('get')
      ->willReturnMap($siteConfigMap);

    $configFactory = $this->getMockBuilder('Drupal\Core\Config\ConfigFactory')
      ->disableOriginalConstructor()
      ->getMock();

    $map = [
      ['service_links.settings', $config],
      ['system.site', $siteConfig],
    ];

    $configFactory->expects($this->any())
      ->method('getEditable')
      ->willReturnMap($map);

    return $configFactory;
  }

  /**
   * Create other config array content.
   */
  private function setOtherConfigItems() {

    $body = new \stdClass();
    $body->summary = 'Summary';

    $otherConfig = [
      'nodeRevisionAuthorId' => 1,
      'nodeRevisionIsPublished' => TRUE,
      'nodeType' => 'article',
      'front_page' => '/node/1',
      'routeName' => 'entity.node.canonical',
      'eid' => 1,
      'entity_type' => 'node',
      'generatedRoute' => 'https://kermit.frog.fr',
      'isFrontPage' => TRUE,
      'body' => $body,
      'settingsKey' => 'service_links.settings',
      'formatterSettings' => [
        'theme_variant' => 'default',
        'link_style' => 3,
      ],
      'canViewServiceLinks' => TRUE,
    ];

    switch ($this->getName()) {
      case 'entityDisplayNotHiddenIfHiddenForAuthorAndNoAuthorMatch':
        $otherConfig['nodeRevisionAuthorId'] = 2;
        break;

      case 'buildDoesntRenderEntityIfNoPermission':
        $otherConfig['canViewServiceLinks'] = FALSE;
        break;

      case 'entityDisplayIsHiddenIfUnpublishedAndHideIfUnpublished':
      case 'buildDoesntRenderEntityIfDisplayDisabled':
        $otherConfig['nodeRevisionIsPublished'] = FALSE;
        break;

      default:
        break;
    }

    $this->otherConfig = $otherConfig;
  }

  /**
   * Create mock request service.
   *
   * @return \PHPUnit_Framework_MockObject_MockObject
   *   Mock request service.
   */
  private function getMockRequestStackService() {
    $request = $this->createMock('\Symfony\Component\HttpFoundation\Request');

    $stack = $this->createMock('\Symfony\Component\HttpFoundation\RequestStack');

    $stack->method('getCurrentRequest')
      ->willReturn($request);

    return $stack;
  }

  /**
   * Create mock current route match service.
   *
   * @return \PHPUnit_Framework_MockObject_MockObject
   *   Mock request service.
   */
  private function getMockCurrentRouteMatchService() {
    $route = $this->createMock('\Symfony\Component\Routing\Route');

    $matcher = $this->createMock('\Drupal\Core\Routing\CurrentRouteMatch');

    $matcher->method('getRouteObject')
      ->willReturn($route);

    return $matcher;
  }

  /**
   * Create mock current path stack service.
   *
   * @return \PHPUnit_Framework_MockObject_MockObject
   *   Mock current path stack service.
   */
  private function getMockCurrentPathStack() {
    $currentPathStack = $this->createMock('\Drupal\Core\Path\CurrentPathStack');

    switch ($this->getName()) {
      default:
        $currentPath = '/awesomeness';
    }

    $currentPathStack->method('getPath')
      ->willReturn($currentPath);

    return $currentPathStack;
  }

  /**
   * Create mock alias manager service.
   *
   * @return \PHPUnit_Framework_MockObject_MockObject
   *   Mock alias manager service.
   */
  private function getMockAliasManager() {
    // Mapping of the current path. Couldn't get it to work with a simple
    // map, possibly due to unicode use? (Hard to debug with mocks).
    $aliasManagerCallback = function ($input) {
      switch ($input) {
        case '/awesomeness':
          return '/node/1';

        default:
          return $input;
      }
    };

    $aliasManagerReverseCallback = function ($input) {
      switch ($input) {
        case '/node/1':
          return '/awesomeness';

        default:
          return $input;
      }
    };

    $aliasManager = $this->getMockBuilder('Drupal\Core\Path\AliasManager')
      ->disableOriginalConstructor()
      ->getMock();

    $aliasManager->expects($this->any())
      ->method('getAliasByPath')
      ->willReturnCallback($aliasManagerCallback);

    $aliasManager
      ->method('getPathByAlias')
      ->willReturnCallback($aliasManagerReverseCallback);

    return $aliasManager;
  }

  /**
   * Get mock title resolver service.
   *
   * @return \PHPUnit_Framework_MockObject_MockObject
   *   Mock title resolver service.
   */
  private function getMockTitleResolver() {
    // Title resolver. Nothing interesting here yet.
    $titleResolver = $this->getMockBuilder('Drupal\Core\Controller\TitleResolver')
      ->disableOriginalConstructor()
      ->getMock();

    $titleResolver
      ->method('getTitle')
      ->willReturn('Resolved Title');

    return $titleResolver;
  }

  /**
   * Get mock path matcher service.
   *
   * @return \PHPUnit_Framework_MockObject_MockObject
   *   Mock path matcher service.
   */
  private function getMockPathMatcher() {
    // Path matcher - does the URL match a list of regexes?
    $pathMatcher = $this->getMockBuilder('Drupal\Core\Path\PathMatcher')
      ->disableOriginalConstructor()
      ->getMock();

    $pathMatcher->method('matchPath')
      ->willReturnMap([
        ['/node/1', '/node/{node}', TRUE],
        ['/awesomeness', '/awesomeness', TRUE],
      ]);

    $pathMatcher->method('isFrontPage')
      ->willReturn($this->otherConfig['isFrontPage']);

    return $pathMatcher;
  }

  /**
   * Get mock current user service.
   *
   * @return \PHPUnit_Framework_MockObject_MockObject
   *   Mock current user service.
   */
  private function getMockCurrentUserService() {
    $currentUser = $this->getMockBuilder('Drupal\Core\Session\AccountProxyInterface')
      ->disableOriginalConstructor()
      ->getMock();

    $currentUser->method('id')
      ->willReturn(1);

    $currentUser->method('hasPermission')
      ->with('access service links')
      ->willReturn($this->otherConfig['canViewServiceLinks']);

    return $currentUser;
  }

  /**
   * Get mock url generator service.
   *
   * @return \PHPUnit_Framework_MockObject_MockObject
   *   Mock URL Generator service.
   */
  private function getMockUrlGenerator() {
    $generateFromRouteCallback = function ($routename, $parameters, $options) {
      if (isset($options['absolute']) && !$options['absolute']) {
        // Dumb bit of code to build a /node/1 relative URL.
        $result = '';
        foreach ($parameters as $key => $value) {
          $result .= "/{$key}/${value}";
        }
        return $result;
      }
      $elements = array_merge($parameters, $options);
      $args = ["routename={$routename}"];
      array_walk_recursive($elements, function ($v, $k) use (&$args) {
        $args[] = "{$k}={$v}";
      });
      return $this->otherConfig['generatedRoute'] . '?' . implode('&amp;', $args);
    };

    $urlGenerator = $this->getMockBuilder('Drupal\Core\Routing\UrlGenerator')
      ->disableOriginalConstructor()
      ->getMock();

    $urlGenerator
      ->method('generateFromRoute')
      ->willReturnCallback($generateFromRouteCallback);

    return $urlGenerator;
  }

  /**
   * Get mock httpClient service.
   *
   * @return \PHPUnit_Framework_MockObject_MockObject
   *   Mock GuzzleHttp client.
   */
  private function getMockHttpClientService() {
    $httpClientCallback = function ($method, array $options = []) {
      $response = $this->getMockBuilder('GuzzleHttp\Psr7\Response')
        ->disableOriginalConstructor()
        ->getMock();

      $parts = explode("url=", $options[0]);

      $response->method('getBody')
        ->willReturn('https://tiny.url?dest=' . $parts[1]);

      return $response;
    };

    $httpClient = $this->getMockBuilder('GuzzleHttp\Client')
      ->disableOriginalConstructor()
      ->getMock();

    $httpClient
      ->method('__call')
      ->willReturnCallback($httpClientCallback);

    return $httpClient;
  }

  /**
   * Mock $entity generation.
   *
   * @return \PHPUnit_Framework_MockObject_MockObject
   *   Mock $entity for testing.
   */
  private function getMockEntity() {

    // Override magic getter so properties will work.
    $methods = [
      'getRevisionUserId',
      'isPublished',
      'getType',
      'getEntityTypeId',
      'label',
      'id',
      'get',
      'hasField',
    ];

    $entity = $this->getMockBuilder('Drupal\Core\Entity\Entity')
      ->setMethods($methods)
      ->disableOriginalConstructor()
      ->getMock();

    $entity->method('getRevisionUserId')
      ->willReturn($this->otherConfig['nodeRevisionAuthorId']);

    $entity->method('label')
      ->willReturn('Original Title');

    $entity->method('isPublished')
      ->willReturn($this->otherConfig['nodeRevisionIsPublished']);

    $entity->method('getType')
      ->willReturn($this->otherConfig['nodeType']);

    $entity->method('getEntityTypeId')
      ->willReturn('node');

    $entity->method('id')
      ->willReturn($this->otherConfig['eid']);

    $entity->method('hasField')
      ->with('body')
      ->willReturn(TRUE);

    $entity->method('get')
      ->with('body')
      ->willReturn($this->otherConfig['body']);

    return $entity;
  }

  /**
   * Mock container $entity generation.
   *
   * @return \PHPUnit_Framework_MockObject_MockObject
   *   Mock $entity for testing.
   */
  private function getMockContainerEntity() {

    // Override magic getter so properties will work.
    $methods = [
      'getEntityTypeId',
    ];

    $entity = $this->getMockBuilder('Drupal\Core\Entity\Entity')
      ->setMethods($methods)
      ->disableOriginalConstructor()
      ->getMock();

    $entity->method('getEntityTypeId')
      ->willReturn('my_block_type');

    return $entity;
  }

  /**
   * Mock service link plugin manager.
   */
  public function getMockServiceLinkPluginManager() {

    $firstInstance = $this->getMockBuilder('Drupal\basque_services\Plugin\ServiceLink\ServiceLinkBase')
      ->setMethods(['getDetails', 'preset_function'])
      ->disableOriginalConstructor()
      ->getMock();

    $firstInstancePresetCallback = function ($service, $tokens, $entity = NULL) {
      $service->detail['name'] = 'Preset Name';
      $settings['subst']['url'] = "//grindstone.com.au";
      return [$service, $tokens];
    };

    $firstInstance->method('getDetails')
      ->willReturn([
        'first' => [
          'link' => '<url>/<title>/<teaser>',
          'description' => 'This is a link',
          'name' => 'First plugin',
          'libraries' => ['first:my_library'],
          'preset' => 'preset_function',
        ],
      ]);

    $firstInstance->method('preset_function')
      ->willReturnCallback($firstInstancePresetCallback);

    $methods = ['getDetails'];

    if ($this->getName() == 'getLinksDoesntReturnPluginsWhereAccessDenied' ||
      $this->getName() == 'getLinksReturnsCacheTagsFromAccessChecks') {
      $methods[] = 'accessCheck';
    }

    $secondInstance = $this->getMockBuilder('Drupal\basque_services\Plugin\ServiceLink\ServiceLinkBase')
      ->setMethods($methods)
      ->disableOriginalConstructor()
      ->getMock();

    $secondDetails = [
      'second_1' => [
        'link' => 'http://somewhere.com?foo=bar',
        'name' => 'Second plugin',
        'description' => 'Foo=bar',
      ],
      'second_2' => [
        'link' => '<front-page>',
        'name' => 'Second plugin',
        'description' => 'A front page link.',
      ],
      'second_3' => [
        'link' => 'http://foo.com',
        'name' => 'Second plugin',
        'description' => 'A boring old link.',
      ],
    ];

    if ($this->getName() == 'getLinksDoesntReturnPluginsWhereAccessDenied' ||
      $this->getName() == 'getLinksReturnsCacheTagsFromAccessChecks') {
      $secondDetails['second_1']['access'] = 'accessCheck';

      $accessCheck = function () {
        return [FALSE, ['testtag']];
      };

      $secondInstance->method('accessCheck')
        ->willReturnCallback($accessCheck);
    }

    $secondInstance->method('getDetails')
      ->willReturn($secondDetails);

    $thirdInstance = $this->getMockBuilder('Drupal\basque_services\Plugin\ServiceLink\ServiceLinkBase')
      ->setMethods(['getDetails'])
      ->disableOriginalConstructor()
      ->getMock();

    $thirdInstance->method('getDetails')
      ->willReturn([
        'third' => [
          'link' => 'http://foo.com',
          'name' => 'Third plugin',
          'description' => 'Another boring old link',
        ],
      ]);

    $definitions = [
      'first' => ['name' => 'name', 'id' => 'id'],
      'second' => ['id' => 'id'],
      'third' => ['id' => 'id'],
    ];

    $instances = [
      'first' => $firstInstance,
      'second' => $secondInstance,
      'third' => $thirdInstance,
    ];

    $createInstanceCallback = function ($arg) use ($instances) {
      return $instances[$arg];
    };

    $manager = $this->createMock('Drupal\service_links\ServiceLinkManager');

    $manager->method('getDefinitions')
      ->willReturn($definitions);

    // Tried willReturnMap, without success here.
    $manager->method('createInstance')
      ->willReturnCallback($createInstanceCallback);

    return $manager;
  }

  /**
   * Mock renderer (for titles).
   */
  public function getMockRenderer() {
    $renderer = $this->createMock('Drupal\Core\Render\Renderer');

    $renderer->method('render')
      ->willReturnArgument(0);

    return $renderer;
  }

  /**
   * Get a mock token service.
   */
  public function getMockTokenService() {

    $tokenCallback = function ($a, $b) {
      return 'Tokens Replaced';
    };

    $token = $this->createMock('Drupal\Core\Utility\Token');

    $token->method('replace')
      ->willReturnCallback($tokenCallback);

    return $token;
  }

  /**
   * Setup for running a test.
   */
  public function setUp() {
    parent::setUp();

    // Set misc configuration values.
    $this->setOtherConfigItems();

    // Build the container that will be used to get services.
    $container = new ContainerBuilder();
    $container->set('module_handler', $this->getMockModuleHandler());
    $container->set('config.factory', $this->getMockConfigFactory());
    $container->set('path.current', $this->getMockCurrentPathStack());
    $container->set('path.alias_manager', $this->getMockAliasManager());
    $container->set('title_resolver', $this->getMockTitleResolver());
    $container->set('path.matcher', $this->getMockPathMatcher());
    $container->set('current_user', $this->getMockCurrentUserService());
    $container->set('url_generator', $this->getMockUrlGenerator());
    $container->set('http_client', $this->getMockHttpClientService());
    $container->set('request_stack', $this->getMockRequestStackService());
    $container->set('current_route_match', $this->getMockCurrentRouteMatchService());
    $container->set('plugin.manager.service_link', $this->getMockServiceLinkPluginManager());
    $container->set('renderer', $this->getMockRenderer());
    $container->set('token', $this->getMockTokenService());
    \Drupal::setContainer($container);

    // Create the service links controller.
    $this->serviceLinksController = ServiceLinksController::create($container, $this->otherConfig['settingsKey'], $this->otherConfig['formatterSettings']);
  }

  /**
   * ExpandPath returns a filename with path components without modification.
   *
   * @test
   */
  public function expandPathDoesntModifyFileWithPath() {

    $expected = 'foo/bar';
    $actual = $this->serviceLinksController->expandPath('foo/bar');

    $this->assertEquals($expected, $actual);
  }

  /**
   * ExpandPath adds the module path and prefix when a simple filename is given.
   *
   * @test
   */
  public function expandPathModifiesFileWithPath() {
    // The mock module path plus 'images/' plus the argument.
    $expected = '/modules/custom/service_links/images/foo';
    $actual = $this->serviceLinksController->expandPath('foo');

    $this->assertEquals($expected, $actual);
  }

  /**
   * ExpandPath gives the default module path when no filename is given.
   *
   * @test
   */
  public function expandPathReturnsDefaultPathWhenNoFilenameGiven() {
    // The mock module path plus 'images/' plus the argument.
    $expected = '/modules/custom/service_links/images';
    $actual = $this->serviceLinksController->expandPath();

    $this->assertEquals($expected, $actual);
  }

  /**
   * ExpandPath applies context argument as expected.
   *
   * (Uses config to distinguish the result from the default value).
   *
   * @test
   */
  public function expandPathAppliesContextArgumentAsExpected() {
    $expected = '/widgets';
    $actual = $this->serviceLinksController->expandPath(NULL, 'widgets');

    $this->assertEquals($expected, $actual);
  }

  /**
   * ExpandPath applies default argument as expected.
   *
   * Uses a previously unseen and unconfigured context to ensure we fall back
   * to the provided default.
   *
   * @test
   */
  public function expandPathAppliesDefaultArgumentAsExpected() {
    $expected = '/modules/custom/service_links/js';
    $actual = $this->serviceLinksController->expandPath(NULL, 'foo', 'javascript');

    $this->assertEquals($expected, $actual);
  }

  /**
   * ExpandPath checks that file exists if so configured.
   *
   * @test
   */
  public function expandPathCanCheckTheFileExists() {
    $expected = '/modules/custom/service_links/tests/src/Unit/assets/foo.txt';
    $actual = $this->serviceLinksController->expandPath('foo.txt', 'check1', 'check1');

    $this->assertEquals($expected, $actual);
  }

  /**
   * ExpandPath falls back to default context if checked asset doesn't exist.
   *
   * @test
   */
  public function expandPathFallsBackToDefaultContextIfTheFileExist() {
    $expected = '/modules/custom/service_links/images/foo.jpg';
    $actual = $this->serviceLinksController->expandPath('foo.jpg', 'check1');

    $this->assertEquals($expected, $actual);
  }

  /**
   * EntityDisplayIsEnabled returns false if hidden for author & author matches.
   *
   * @test
   */
  public function entityDisplayIsHiddenIfHiddenForAuthorAndAuthorMatches() {
    $actual = $this->serviceLinksController->entityDisplayIsEnabled($this->getMockEntity());
    $this->assertEquals(FALSE, $actual);
  }

  /**
   * EntityDisplayIsEnabled returns true if hidden for author & no author match.
   *
   * @test
   */
  public function entityDisplayNotHiddenIfHiddenForAuthorAndNoAuthorMatch() {
    $actual = $this->serviceLinksController->entityDisplayIsEnabled($this->getMockEntity());
    $this->assertEquals(TRUE, $actual);
  }

  /**
   * EntityDisplayIsEnabled checks publication status if requested.
   *
   * @test
   */
  public function entityDisplayIsHiddenIfUnpublishedAndHideIfUnpublished() {
    $actual = $this->serviceLinksController->entityDisplayIsEnabled($this->getMockEntity());
    $this->assertEquals(FALSE, $actual);
  }

  /**
   * EntityDisplayIsEnabled doesn't hide published nodes.
   *
   * @test
   */
  public function entityDisplayIsHiddenDoesNotHidePublishedEntitysWithHideIfUnpublished() {
    $actual = $this->serviceLinksController->entityDisplayIsEnabled($this->getMockEntity());
    $this->assertEquals(TRUE, $actual);
  }

  /**
   * GetEntityLabel returns default title if no override set.
   *
   * @test
   */
  public function getEntityLabelReturnsDefaultTitleIfNoOverrideSet() {
    $actual = $this->serviceLinksController->getEntityLabel($this->getMockEntity());
    $this->assertEquals('Original Title', $actual);
  }

  /**
   * GetEntityLabel returns overridden title if configured to do so.
   *
   * @test
   */
  public function getEntityLabelReturnsOverridenTitleIfConfiguredToDoSo() {
    $actual = $this->serviceLinksController->getEntityLabel($this->getMockEntity());
    $this->assertEquals('Replacement Title', $actual);
  }

  /**
   * GetEntityLabel returns tokenised title if configured to do so.
   *
   * @test
   */
  public function getEntityLabelReturnsTokenisedTitleIfConfiguredToDoSo() {
    $actual = $this->serviceLinksController->getEntityLabel($this->getMockEntity());
    $this->assertEquals('Tokens Replaced', $actual);
  }

  /**
   * GetQuery returns NULL when passed an empty string.
   *
   * @test
   */
  public function getQueryReturnsNullForAnEmptyString() {
    $actual = $this->serviceLinksController->getQuery('');
    $this->assertEquals(NULL, $actual);
  }

  /**
   * GetQuery returns the correct result for a query string with one part.
   *
   * @test
   */
  public function getQueryHandlesSinglePartQueryCorrectly() {
    $actual = $this->serviceLinksController->getQuery('foo=bar');
    $this->assertEquals(['foo' => 'bar'], $actual);
  }

  /**
   * GetQuery returns the correct result for a string with no value.
   *
   * @test
   */
  public function getQueryHandlesSinglePartWithNoValueQueryCorrectly() {
    $actual = $this->serviceLinksController->getQuery('foo');
    $this->assertEquals(['foo' => ''], $actual);
  }

  /**
   * GetQuery returns the correct result for a complex multipart query.
   *
   * @test
   */
  public function getQueryHandlesComplexMultipartQueriesCorrectly() {
    $actual = $this->serviceLinksController->getQuery('foo&bar&baz=2');
    $this->assertEquals(['foo' => '', 'bar' => '', 'baz' => 2], $actual);
  }

  /**
   * ShortUrl returns the original URL for a non entity using
   * SHORT_URL_TYPE_ENTITY.
   *
   * @test
   */
  public function shortUrlReturnsOriginalUrlForNonEntityUsingShortUrlTypeEntity() {
    $actual = $this->serviceLinksController->shortUrl('/admin');
    $this->assertEquals(['/admin', []], $actual);
  }

  /**
   * ShortUrl returns the original URL for non entity using
   * SHORT_URL_TYPE_ENTITY.
   *
   * @test
   */
  public function shortUrlReturnsAbsoluteAliasedUsingShortUrlTypeEntity() {
    $actual = $this->serviceLinksController->shortUrl('/node/1',
      $this->otherConfig['entity_type'], $this->otherConfig['eid']);

    $this->assertEquals([
      'https://kermit.frog.fr?routename=entity.node.canonical&amp;node=1&amp;absolute=1&amp;alias=1',
      [],
    ], $actual);
  }

  /**
   * Uses shorten module if available when using SHORT_URL_TYPE_SERVICE.
   *
   * @test
   */
  public function shortUrlUsesShortenModuleIfAvailableAndSelected() {
    $actual = $this->serviceLinksController->shortUrl('https://drupal8.local',
      $this->otherConfig['entity_type'], $this->otherConfig['eid']);

    // Yes, I know the destination would be encoded in reality.
    $expected = [
      'https://shortened.url?dest=https://drupal8.local',
      [
        0 => 'config:core.extension',
      ],
    ];
    $this->assertEquals($expected, $actual);
  }

  /**
   * ShortUrl falls back to TinyURL when using SHORT_URL_TYPE_SERVICE.
   *
   * @test
   */
  public function shortUrlFallsBackToTinyUrlIfShortenUnavailable() {
    $actual = $this->serviceLinksController->shortUrl('https://drupal8.local',
      $this->otherConfig['entity_type'], $this->otherConfig['eid']);

    $expected = [
      'https://tiny.url?dest=https%3A%2F%2Fdrupal8.local',
      [
        0 => 'config:core.extension',
      ],
    ];
    $this->assertEquals($expected, $actual);
  }

  /**
   * ShortUrl replaces base URL when using SHORT_URL_REDIRECT_DOMAIN.
   *
   * @test
   */
  public function shortUrlCanReplaceBaseDomain() {
    $actual = $this->serviceLinksController->shortUrl('https://drupal8.local',
      $this->otherConfig['entity_type'], $this->otherConfig['eid']);

    $expected = [
      'https://kermit.frog.fr?routename=entity.node.canonical&amp;node=1&amp;absolute=1&amp;base_url=https://newdomain.net',
      [],
    ];
    $this->assertEquals($expected, $actual);
  }

  /**
   * ShortUrl replaces base URL when using SHORT_URL_REDIRECT_DOMAIN.
   *
   * @test
   */
  public function redirectAllReplacesBaseDomainWithoutEntity() {
    $actual = $this->serviceLinksController->shortUrl('https://drupal8.local');

    $expected = [
      'https://kermit.frog.fr?routename=entity.node.canonical&amp;absolute=1&amp;base_url=https://newdomain.net',
      [],
    ];
    $this->assertEquals($expected, $actual);
  }

  /**
   * ShortUrl replaces base URL when using SHORT_URL_REDIRECT_DOMAIN.
   *
   * @test
   */
  public function redirectAllReplacesBaseDomainWithEntity() {
    $actual = $this->serviceLinksController->shortUrl('https://drupal8.local',
      $this->otherConfig['entity_type'], $this->otherConfig['eid']);

    $expected = [
      'https://kermit.frog.fr?routename=entity.node.canonical&amp;node=1&amp;absolute=1&amp;alias=1&amp;base_url=https://newdomain.net',
      [],
    ];

    $this->assertEquals($expected, $actual);
  }

  /**
   * GetTags correctly calculates the substitutions for a entity.
   *
   * @test
   */
  public function getTokensCorrectlyCalculatesTokenReplacements() {

    list($actual, $tags) = $this->serviceLinksController->getTokens($this->getMockEntity());

    $expected = [
      'tag' => [
        'raw-encoded-title' => '<raw-encoded-title>',
        'raw-encoded-url' => '<raw-encoded-url>',
        'raw-encoded-teaser' => '<raw-encoded-teaser>',
        'raw-encoded-short-url' => '<raw-encoded-short-url>',
        'raw-encoded-long-url' => '<raw-encoded-long-url>',
        'raw-encoded-query' => '<raw-encoded-query>',
        'raw-encoded-source' => '<raw-encoded-source>',
        'encoded-title' => '<encoded-title>',
        'encoded-url' => '<encoded-url>',
        'encoded-teaser' => '<encoded-teaser>',
        'encoded-short-url' => '<encoded-short-url>',
        'encoded-long-url' => '<encoded-long-url>',
        'encoded-query' => '<encoded-query>',
        'encoded-source' => '<encoded-source>',
        'teaser' => '<teaser>',
        'short-url' => '<short-url>',
        'long-url' => '<long-url>',
        'source' => '<source>',
        'entity-id' => '<entity-id>',
        'query' => '<query>',
        'url' => '<url>',
        'title' => '<title>',
        'front-page' => '<front-page>',
        'entity-type' => '<entity-type>',
      ],
      'subst' => [
        'raw-encoded-title' => 'Original Title',
        'raw-encoded-url' => 'https://kermit.frog.fr?routename=entity.node.canonical&amp;node=1&amp;absolute=1&amp;appended=true',
        'raw-encoded-teaser' => 'Summary',
        'raw-encoded-short-url' => 'https://kermit.frog.fr?routename=entity.node.canonical&amp;node=1&amp;absolute=1&amp;appended=true',
        'raw-encoded-long-url' => 'https://kermit.frog.fr?routename=entity.node.canonical&amp;node=1&amp;absolute=1&amp;appended=true',
        'raw-encoded-query' => '/node/1',
        'raw-encoded-source' => 'Site Name',
        'encoded-title' => 'Original Title',
        'encoded-url' => 'https://kermit.frog.fr?routename=entity.node.canonical&amp;amp;node=1&amp;amp;absolute=1&amp;amp;appended=true',
        'encoded-teaser' => 'Summary',
        'encoded-short-url' => 'https://kermit.frog.fr?routename=entity.node.canonical&amp;amp;node=1&amp;amp;absolute=1&amp;amp;appended=true',
        'encoded-long-url' => 'https://kermit.frog.fr?routename=entity.node.canonical&amp;amp;node=1&amp;amp;absolute=1&amp;amp;appended=true',
        'encoded-query' => '/node/1',
        'encoded-source' => 'Site Name',
        'teaser' => 'Summary',
        'short-url' => 'https://kermit.frog.fr?routename=entity.node.canonical&amp;node=1&amp;absolute=1&amp;appended=true',
        'long-url' => 'https://kermit.frog.fr?routename=entity.node.canonical&amp;node=1&amp;absolute=1&amp;appended=true',
        'source' => 'Site Name',
        'entity-id' => 1,
        'query' => '/node/1',
        'url' => 'https://kermit.frog.fr?routename=entity.node.canonical&amp;node=1&amp;absolute=1&amp;appended=true',
        'title' => 'Original Title',
        'front-page' => 'https://kermit.frog.fr?routename=<front>&amp;absolute=1',
        'entity-type' => 'node',
      ],
    ];

    $this->assertEquals($expected, $actual);
  }

  /**
   * GetTags correctly calculates the substitutions where no entity is provided.
   *
   * @test
   */
  public function getTokensCorrectlyCalculatesTokenReplacementsWithoutEntity() {

    list($actual, $tags) = $this->serviceLinksController->getTokens(NULL);

    $expected = [
      'tag' => [
        'raw-encoded-title' => '<raw-encoded-title>',
        'raw-encoded-url' => '<raw-encoded-url>',
        'raw-encoded-teaser' => '<raw-encoded-teaser>',
        'raw-encoded-short-url' => '<raw-encoded-short-url>',
        'raw-encoded-long-url' => '<raw-encoded-long-url>',
        'raw-encoded-query' => '<raw-encoded-query>',
        'raw-encoded-source' => '<raw-encoded-source>',
        'encoded-title' => '<encoded-title>',
        'encoded-url' => '<encoded-url>',
        'encoded-teaser' => '<encoded-teaser>',
        'encoded-short-url' => '<encoded-short-url>',
        'encoded-long-url' => '<encoded-long-url>',
        'encoded-query' => '<encoded-query>',
        'encoded-source' => '<encoded-source>',
        'teaser' => '<teaser>',
        'short-url' => '<short-url>',
        'long-url' => '<long-url>',
        'source' => '<source>',
        'entity-id' => '<entity-id>',
        'query' => '<query>',
        'url' => '<url>',
        'title' => '<title>',
        'front-page' => '<front-page>',
        'entity-type' => '<entity-type>',
      ],
      'subst' => [
        'raw-encoded-title' => 'Resolved Title',
        'raw-encoded-url' => 'https://kermit.frog.fr?routename=<front>&amp;absolute=1&amp;appended=true',
        'raw-encoded-teaser' => '',
        'raw-encoded-short-url' => 'https://kermit.frog.fr?routename=<front>&amp;absolute=1&amp;appended=true',
        'raw-encoded-long-url' => 'https://kermit.frog.fr?routename=<front>&amp;absolute=1&amp;appended=true',
        'raw-encoded-query' => '/awesomeness',
        'raw-encoded-source' => 'Site Name',
        'encoded-title' => 'Resolved Title',
        'encoded-url' => 'https://kermit.frog.fr?routename=&lt;front&gt;&amp;amp;absolute=1&amp;amp;appended=true',
        'encoded-teaser' => '',
        'encoded-short-url' => 'https://kermit.frog.fr?routename=&lt;front&gt;&amp;amp;absolute=1&amp;amp;appended=true',
        'encoded-long-url' => 'https://kermit.frog.fr?routename=&lt;front&gt;&amp;amp;absolute=1&amp;amp;appended=true',
        'encoded-query' => '/awesomeness',
        'encoded-source' => 'Site Name',
        'teaser' => '',
        'short-url' => 'https://kermit.frog.fr?routename=<front>&amp;absolute=1&amp;appended=true',
        'long-url' => 'https://kermit.frog.fr?routename=<front>&amp;absolute=1&amp;appended=true',
        'source' => 'Site Name',
        'entity-id' => NULL,
        'query' => '/awesomeness',
        'url' => 'https://kermit.frog.fr?routename=<front>&amp;absolute=1&amp;appended=true',
        'title' => 'Resolved Title',
        'front-page' => 'https://kermit.frog.fr?routename=<front>&amp;absolute=1',
        'entity-type' => '',
      ],
    ];

    $this->assertEquals($expected, $actual);
  }

  /**
   * GetTags correctly calculates the tokens where no entity is provided.
   *
   * @test
   */
  public function getTokensAppliesShortUrlWhenRequestedSettingWithNoNid() {

    list($actual, $tags) = $this->serviceLinksController->getTokens(NULL);

    $expected = [
      'tag' => [
        'raw-encoded-title' => '<raw-encoded-title>',
        'raw-encoded-url' => '<raw-encoded-url>',
        'raw-encoded-teaser' => '<raw-encoded-teaser>',
        'raw-encoded-short-url' => '<raw-encoded-short-url>',
        'raw-encoded-long-url' => '<raw-encoded-long-url>',
        'raw-encoded-query' => '<raw-encoded-query>',
        'raw-encoded-source' => '<raw-encoded-source>',
        'encoded-title' => '<encoded-title>',
        'encoded-url' => '<encoded-url>',
        'encoded-teaser' => '<encoded-teaser>',
        'encoded-short-url' => '<encoded-short-url>',
        'encoded-long-url' => '<encoded-long-url>',
        'encoded-query' => '<encoded-query>',
        'encoded-source' => '<encoded-source>',
        'teaser' => '<teaser>',
        'short-url' => '<short-url>',
        'long-url' => '<long-url>',
        'source' => '<source>',
        'entity-id' => '<entity-id>',
        'query' => '<query>',
        'url' => '<url>',
        'title' => '<title>',
        'front-page' => '<front-page>',
        'entity-type' => '<entity-type>',
      ],
      'subst' => [
        'raw-encoded-title' => 'Resolved Title',
        'raw-encoded-url' => 'https://kermit.frog.fr?routename=<front>&amp;absolute=1&amp;appended=true',
        'raw-encoded-teaser' => '',
        'raw-encoded-short-url' => 'https://tiny.url?dest=https%3A%2F%2Fkermit.frog.fr%3Froutename%3D%3Cfront%3E%26amp%3Babsolute%3D1%26amp%3Bappended%3Dtrue',
        'raw-encoded-long-url' => 'https://kermit.frog.fr?routename=<front>&amp;absolute=1&amp;appended=true',
        'raw-encoded-query' => '/awesomeness',
        'raw-encoded-source' => 'Site Name',
        'encoded-title' => 'Resolved Title',
        'encoded-url' => 'https://kermit.frog.fr?routename=&lt;front&gt;&amp;amp;absolute=1&amp;amp;appended=true',
        'encoded-teaser' => '',
        'encoded-short-url' => 'https://tiny.url?dest=https%3A%2F%2Fkermit.frog.fr%3Froutename%3D%3Cfront%3E%26amp%3Babsolute%3D1%26amp%3Bappended%3Dtrue',
        'encoded-long-url' => 'https://kermit.frog.fr?routename=&lt;front&gt;&amp;amp;absolute=1&amp;amp;appended=true',
        'encoded-query' => '/awesomeness',
        'encoded-source' => 'Site Name',
        'teaser' => '',
        'short-url' => 'https://tiny.url?dest=https%3A%2F%2Fkermit.frog.fr%3Froutename%3D%3Cfront%3E%26amp%3Babsolute%3D1%26amp%3Bappended%3Dtrue',
        'long-url' => 'https://kermit.frog.fr?routename=<front>&amp;absolute=1&amp;appended=true',
        'source' => 'Site Name',
        'entity-id' => NULL,
        'query' => '/awesomeness',
        'url' => 'https://kermit.frog.fr?routename=<front>&amp;absolute=1&amp;appended=true',
        'title' => 'Resolved Title',
        'front-page' => 'https://kermit.frog.fr?routename=<front>&amp;absolute=1',
        'entity-type' => '',
      ],
    ];

    $this->assertEquals($expected, $actual);
  }

  /**
   * GetTags correctly calculates tokens where 'when requested' is the setting.
   *
   * @test
   */
  public function getTokensAppliesShortUrlWhenRequestedSettingWithNid() {

    list($actual, $tags) = $this->serviceLinksController->getTokens($this->getMockEntity());

    $expected = [
      'tag' => [
        'raw-encoded-title' => '<raw-encoded-title>',
        'raw-encoded-url' => '<raw-encoded-url>',
        'raw-encoded-teaser' => '<raw-encoded-teaser>',
        'raw-encoded-short-url' => '<raw-encoded-short-url>',
        'raw-encoded-long-url' => '<raw-encoded-long-url>',
        'raw-encoded-query' => '<raw-encoded-query>',
        'raw-encoded-source' => '<raw-encoded-source>',
        'encoded-title' => '<encoded-title>',
        'encoded-url' => '<encoded-url>',
        'encoded-teaser' => '<encoded-teaser>',
        'encoded-short-url' => '<encoded-short-url>',
        'encoded-long-url' => '<encoded-long-url>',
        'encoded-query' => '<encoded-query>',
        'encoded-source' => '<encoded-source>',
        'teaser' => '<teaser>',
        'short-url' => '<short-url>',
        'long-url' => '<long-url>',
        'source' => '<source>',
        'entity-id' => '<entity-id>',
        'query' => '<query>',
        'url' => '<url>',
        'title' => '<title>',
        'front-page' => '<front-page>',
        'entity-type' => '<entity-type>',
      ],
      'subst' => [
        'raw-encoded-title' => 'Original Title',
        'raw-encoded-url' => 'https://kermit.frog.fr?routename=entity.node.canonical&amp;node=1&amp;absolute=1&amp;appended=true',
        'raw-encoded-teaser' => 'Summary',
        'raw-encoded-short-url' => 'https://shortened.url?dest=https://kermit.frog.fr?routename=entity.node.canonical&amp;node=1&amp;absolute=1&amp;appended=true',
        'raw-encoded-long-url' => 'https://kermit.frog.fr?routename=entity.node.canonical&amp;node=1&amp;absolute=1&amp;appended=true',
        'raw-encoded-query' => '/node/1',
        'raw-encoded-source' => 'Site Name',
        'encoded-title' => 'Original Title',
        'encoded-url' => 'https://kermit.frog.fr?routename=entity.node.canonical&amp;amp;node=1&amp;amp;absolute=1&amp;amp;appended=true',
        'encoded-teaser' => 'Summary',
        'encoded-short-url' => 'https://shortened.url?dest=https://kermit.frog.fr?routename=entity.node.canonical&amp;amp;node=1&amp;amp;absolute=1&amp;amp;appended=true',
        'encoded-long-url' => 'https://kermit.frog.fr?routename=entity.node.canonical&amp;amp;node=1&amp;amp;absolute=1&amp;amp;appended=true',
        'encoded-query' => '/node/1',
        'encoded-source' => 'Site Name',
        'teaser' => 'Summary',
        'short-url' => 'https://shortened.url?dest=https://kermit.frog.fr?routename=entity.node.canonical&amp;node=1&amp;absolute=1&amp;appended=true',
        'long-url' => 'https://kermit.frog.fr?routename=entity.node.canonical&amp;node=1&amp;absolute=1&amp;appended=true',
        'source' => 'Site Name',
        'entity-id' => 1,
        'query' => '/node/1',
        'url' => 'https://kermit.frog.fr?routename=entity.node.canonical&amp;node=1&amp;absolute=1&amp;appended=true',
        'title' => 'Original Title',
        'front-page' => 'https://kermit.frog.fr?routename=<front>&amp;absolute=1',
        'entity-type' => 'node',
      ],
    ];

    $this->assertEquals($expected, $actual);
  }

  /**
   * GetTags puts short URL in both tokens when requested.
   *
   * @test
   */
  public function getTokensAlwaysAppliesShortUrlWhenRequestedWithNoNid() {

    list($actual, $tags) = $this->serviceLinksController->getTokens(NULL);

    $expected = [
      'tag' => [
        'raw-encoded-title' => '<raw-encoded-title>',
        'raw-encoded-url' => '<raw-encoded-url>',
        'raw-encoded-teaser' => '<raw-encoded-teaser>',
        'raw-encoded-short-url' => '<raw-encoded-short-url>',
        'raw-encoded-long-url' => '<raw-encoded-long-url>',
        'raw-encoded-query' => '<raw-encoded-query>',
        'raw-encoded-source' => '<raw-encoded-source>',
        'encoded-title' => '<encoded-title>',
        'encoded-url' => '<encoded-url>',
        'encoded-teaser' => '<encoded-teaser>',
        'encoded-short-url' => '<encoded-short-url>',
        'encoded-long-url' => '<encoded-long-url>',
        'encoded-query' => '<encoded-query>',
        'encoded-source' => '<encoded-source>',
        'teaser' => '<teaser>',
        'short-url' => '<short-url>',
        'long-url' => '<long-url>',
        'source' => '<source>',
        'entity-id' => '<entity-id>',
        'query' => '<query>',
        'url' => '<url>',
        'title' => '<title>',
        'front-page' => '<front-page>',
        'entity-type' => '<entity-type>',
      ],
      'subst' => [
        'raw-encoded-title' => 'Resolved Title',
        'raw-encoded-url' => 'https://tiny.url?dest=https%3A%2F%2Fkermit.frog.fr%3Froutename%3D%3Cfront%3E%26amp%3Babsolute%3D1%26amp%3Bappended%3Dtrue',
        'raw-encoded-teaser' => '',
        'raw-encoded-short-url' => 'https://tiny.url?dest=https%3A%2F%2Fkermit.frog.fr%3Froutename%3D%3Cfront%3E%26amp%3Babsolute%3D1%26amp%3Bappended%3Dtrue',
        'raw-encoded-long-url' => 'https://kermit.frog.fr?routename=<front>&amp;absolute=1&amp;appended=true',
        'raw-encoded-query' => '/awesomeness',
        'raw-encoded-source' => 'Site Name',
        'encoded-title' => 'Resolved Title',
        'encoded-url' => 'https://tiny.url?dest=https%3A%2F%2Fkermit.frog.fr%3Froutename%3D%3Cfront%3E%26amp%3Babsolute%3D1%26amp%3Bappended%3Dtrue',
        'encoded-teaser' => '',
        'encoded-short-url' => 'https://tiny.url?dest=https%3A%2F%2Fkermit.frog.fr%3Froutename%3D%3Cfront%3E%26amp%3Babsolute%3D1%26amp%3Bappended%3Dtrue',
        'encoded-long-url' => 'https://kermit.frog.fr?routename=&lt;front&gt;&amp;amp;absolute=1&amp;amp;appended=true',
        'encoded-query' => '/awesomeness',
        'encoded-source' => 'Site Name',
        'teaser' => '',
        'short-url' => 'https://tiny.url?dest=https%3A%2F%2Fkermit.frog.fr%3Froutename%3D%3Cfront%3E%26amp%3Babsolute%3D1%26amp%3Bappended%3Dtrue',
        'long-url' => 'https://kermit.frog.fr?routename=<front>&amp;absolute=1&amp;appended=true',
        'source' => 'Site Name',
        'entity-id' => NULL,
        'query' => '/awesomeness',
        'url' => 'https://tiny.url?dest=https%3A%2F%2Fkermit.frog.fr%3Froutename%3D%3Cfront%3E%26amp%3Babsolute%3D1%26amp%3Bappended%3Dtrue',
        'title' => 'Resolved Title',
        'front-page' => 'https://kermit.frog.fr?routename=<front>&amp;absolute=1',
        'entity-type' => '',
      ],
    ];

    $this->assertEquals($expected, $actual);
  }

  /**
   * GetTags puts short URL in both tokens when requested.
   *
   * @test
   */
  public function getTokensAlwaysAppliesShortUrlWhenRequestedWithNid() {

    list($actual, $tags) = $this->serviceLinksController->getTokens($this->getMockEntity());

    $expected = [
      'tag' => [
        'raw-encoded-title' => '<raw-encoded-title>',
        'raw-encoded-url' => '<raw-encoded-url>',
        'raw-encoded-teaser' => '<raw-encoded-teaser>',
        'raw-encoded-short-url' => '<raw-encoded-short-url>',
        'raw-encoded-long-url' => '<raw-encoded-long-url>',
        'raw-encoded-query' => '<raw-encoded-query>',
        'raw-encoded-source' => '<raw-encoded-source>',
        'encoded-title' => '<encoded-title>',
        'encoded-url' => '<encoded-url>',
        'encoded-teaser' => '<encoded-teaser>',
        'encoded-short-url' => '<encoded-short-url>',
        'encoded-long-url' => '<encoded-long-url>',
        'encoded-query' => '<encoded-query>',
        'encoded-source' => '<encoded-source>',
        'teaser' => '<teaser>',
        'short-url' => '<short-url>',
        'long-url' => '<long-url>',
        'source' => '<source>',
        'entity-id' => '<entity-id>',
        'query' => '<query>',
        'url' => '<url>',
        'title' => '<title>',
        'front-page' => '<front-page>',
        'entity-type' => '<entity-type>',
      ],
      'subst' => [
        'raw-encoded-title' => 'Original Title',
        'raw-encoded-url' => 'https://tiny.url?dest=https%3A%2F%2Fkermit.frog.fr%3Froutename%3Dentity.node.canonical%26amp%3Bnode%3D1%26amp%3Babsolute%3D1%26amp%3Bappended%3Dtrue',
        'raw-encoded-teaser' => 'Summary',
        'raw-encoded-short-url' => 'https://tiny.url?dest=https%3A%2F%2Fkermit.frog.fr%3Froutename%3Dentity.node.canonical%26amp%3Bnode%3D1%26amp%3Babsolute%3D1%26amp%3Bappended%3Dtrue',
        'raw-encoded-long-url' => 'https://kermit.frog.fr?routename=entity.node.canonical&amp;node=1&amp;absolute=1&amp;appended=true',
        'raw-encoded-query' => '/node/1',
        'raw-encoded-source' => 'Site Name',
        'encoded-title' => 'Original Title',
        'encoded-url' => 'https://tiny.url?dest=https%3A%2F%2Fkermit.frog.fr%3Froutename%3Dentity.node.canonical%26amp%3Bnode%3D1%26amp%3Babsolute%3D1%26amp%3Bappended%3Dtrue',
        'encoded-teaser' => 'Summary',
        'encoded-short-url' => 'https://tiny.url?dest=https%3A%2F%2Fkermit.frog.fr%3Froutename%3Dentity.node.canonical%26amp%3Bnode%3D1%26amp%3Babsolute%3D1%26amp%3Bappended%3Dtrue',
        'encoded-long-url' => 'https://kermit.frog.fr?routename=entity.node.canonical&amp;amp;node=1&amp;amp;absolute=1&amp;amp;appended=true',
        'encoded-query' => '/node/1',
        'encoded-source' => 'Site Name',
        'teaser' => 'Summary',
        'short-url' => 'https://tiny.url?dest=https%3A%2F%2Fkermit.frog.fr%3Froutename%3Dentity.node.canonical%26amp%3Bnode%3D1%26amp%3Babsolute%3D1%26amp%3Bappended%3Dtrue',
        'long-url' => 'https://kermit.frog.fr?routename=entity.node.canonical&amp;node=1&amp;absolute=1&amp;appended=true',
        'source' => 'Site Name',
        'entity-id' => 1,
        'query' => '/node/1',
        'url' => 'https://tiny.url?dest=https%3A%2F%2Fkermit.frog.fr%3Froutename%3Dentity.node.canonical%26amp%3Bnode%3D1%26amp%3Babsolute%3D1%26amp%3Bappended%3Dtrue',
        'title' => 'Original Title',
        'front-page' => 'https://kermit.frog.fr?routename=<front>&amp;absolute=1',
        'entity-type' => 'node',
      ],
    ];

    $this->assertEquals($expected, $actual);
  }

  /**
   * GetTags uses settings 'data' element if supplied and no node given.
   *
   * @test
   */
  public function getsTagsUsesSettingsDataIfSuppliedAndNoEntityGiven() {

    $data = [
      'title' => 'This is a title',
      'url' => 'http://somewhere.net',
      'query' => 'testing=foo',
      'teaser' => 'Teaser',
      'node_id' => 345,
    ];

    list($actual, $tags) = $this->serviceLinksController->getTokens($data);

    $expected = [
      'tag' => [
        'raw-encoded-title' => '<raw-encoded-title>',
        'raw-encoded-url' => '<raw-encoded-url>',
        'raw-encoded-teaser' => '<raw-encoded-teaser>',
        'raw-encoded-short-url' => '<raw-encoded-short-url>',
        'raw-encoded-long-url' => '<raw-encoded-long-url>',
        'raw-encoded-query' => '<raw-encoded-query>',
        'raw-encoded-source' => '<raw-encoded-source>',
        'encoded-title' => '<encoded-title>',
        'encoded-url' => '<encoded-url>',
        'encoded-teaser' => '<encoded-teaser>',
        'encoded-short-url' => '<encoded-short-url>',
        'encoded-long-url' => '<encoded-long-url>',
        'encoded-query' => '<encoded-query>',
        'encoded-source' => '<encoded-source>',
        'teaser' => '<teaser>',
        'short-url' => '<short-url>',
        'long-url' => '<long-url>',
        'source' => '<source>',
        'entity-id' => '<entity-id>',
        'query' => '<query>',
        'url' => '<url>',
        'title' => '<title>',
        'front-page' => '<front-page>',
        'entity-type' => '<entity-type>',
      ],
      'subst' => [
        'raw-encoded-title' => 'This is a title',
        'raw-encoded-url' => 'http://somewhere.net',
        'raw-encoded-teaser' => 'Teaser',
        'raw-encoded-short-url' => 'http://somewhere.net',
        'raw-encoded-long-url' => 'http://somewhere.net',
        'raw-encoded-query' => 'testing=foo',
        'raw-encoded-source' => 'Site Name',
        'encoded-title' => 'This is a title',
        'encoded-url' => 'http://somewhere.net',
        'encoded-teaser' => 'Teaser',
        'encoded-short-url' => 'http://somewhere.net',
        'encoded-long-url' => 'http://somewhere.net',
        'encoded-query' => 'testing=foo',
        'encoded-source' => 'Site Name',
        'teaser' => 'Teaser',
        'short-url' => 'http://somewhere.net',
        'long-url' => 'http://somewhere.net',
        'source' => 'Site Name',
        'entity-id' => 345,
        'query' => 'testing=foo',
        'url' => 'http://somewhere.net',
        'title' => 'This is a title',
        'front-page' => 'https://kermit.frog.fr?routename=<front>&amp;absolute=1',
        'entity-type' => 'node',
      ],
    ];

    $this->assertEquals($expected, $actual);
  }

  /**
   * GetLinks doesn't return disabled plugins.
   *
   * @test
   */
  public function getLinksDoesntReturnPluginsWhereAccessDenied() {
    list($actual, $tags) = $this->serviceLinksController->getLinks(NULL, []);

    $this->assertEquals(3, count($actual));
  }

  /**
   * GetLinks returns cache tags from access checks.
   *
   * @test
   */
  public function getLinksReturnsCacheTagsFromAccessChecks() {
    list($actual, $tags) = $this->serviceLinksController->getLinks(NULL, []);

    $this->assertEquals(['testtag'], $tags);
  }

  /**
   * GetLinks doesn't return disabled plugins.
   *
   * @test
   */
  public function getLinksOrdersPluginsByWeight() {
    list($actual, $tags) = $this->serviceLinksController->getLinks(NULL, []);

    $this->assertEquals(10, $actual[0]->weight);
    $this->assertEquals(19, $actual[1]->weight);
    $this->assertEquals(19, $actual[2]->weight);
    $this->assertEquals(20, $actual[3]->weight);
  }

  /**
   * GetLinks doesn't return disabled plugins.
   *
   * @test
   */
  public function getLinksPrefersPluginDefinitionNameOverId() {
    list($actual, $tags) = $this->serviceLinksController->getLinks(NULL, []);

    $this->assertEquals('name', $actual[0]->group);
    $this->assertEquals('id', $actual[1]->group);
    $this->assertEquals('id', $actual[2]->group);
    $this->assertEquals('id', $actual[3]->group);
  }

  /**
   * GetLinks builds the expected array of objects.
   *
   * @test
   */
  public function getLinksReturnsLinksForEnabledPlugins() {
    list($actual, $tags) = $this->serviceLinksController->getLinks(NULL, []);

    for ($i = 0; $i <= 3; $i++) {
      $this->assertObjectHasAttribute('plugin', $actual[$i]);
      unset($actual[$i]->plugin);
    }

    $expected = [
      (object) [
        'id' => 'first',
        'detail' => [
          'link' => '<url>/<title>/<teaser>',
          'name' => 'First plugin',
          'description' => 'This is a link',
          'libraries' => ['first:my_library'],
          'preset' => 'preset_function',
        ],
        'show' => TRUE,
        'weight' => 10,
        'service_id' => 'first',
        'plugin_definition' => [
          'name' => 'name',
          'id' => 'id',
        ],
        'group' => 'name',
      ],
      (object) [
        'id' => 'second_2',
        'detail' => [
          'link' => '<front-page>',
          'name' => 'Second plugin',
          'description' => 'A front page link.',
        ],
        'show' => TRUE,
        'weight' => 19,
        'service_id' => 'second',
        'plugin_definition' => [
          'id' => 'id',
        ],
        'group' => 'id',
      ],
      (object) [
        'id' => 'second_3',
        'detail' => [
          'link' => 'http://foo.com',
          'name' => 'Second plugin',
          'description' => 'A boring old link.',
        ],
        'show' => TRUE,
        'weight' => 19,
        'service_id' => 'second',
        'plugin_definition' => [
          'id' => 'id',
        ],
        'group' => 'id',
      ],
      (object) [
        'id' => 'second_1',
        'detail' => [
          'link' => 'http://somewhere.com?foo=bar',
          'name' => 'Second plugin',
          'description' => 'Foo=bar',
        ],
        'show' => TRUE,
        'weight' => 20,
        'service_id' => 'second',
        'plugin_definition' => [
          'id' => 'id',
        ],
        'group' => 'id',
      ],
    ];

    $this->assertEquals($expected, $actual);
  }

  /**
   * Render a link.
   *
   * This tests a lot of stuff at once at the moment. I'll introduce
   * extra tests as part of the rework, which I'm starting to think
   * about in earnest now.
   *
   * @test
   */
  public function renderOneProducesExpectedRenderArray() {
    list($tokens, $cache_tags) = $this->serviceLinksController->getTokens(NULL);

    $node = $this->getMockEntity();
    list($services, $tags) = $this->serviceLinksController->getLinks($node, []);
    $cache_tags = array_merge($cache_tags, $tags);

    $actual = $this->serviceLinksController->renderOne('first', $services[0], $node, $tokens);
    $this->assertInstanceOf('Drupal\Core\Url', $actual['first']['#url']);
    unset($actual['first']['#url']);

    $expected = [
      'first' => [
        '#type' => 'service_link',
        '#attached' => [
          'library' => [],
          'drupalSettings' => [],
        ],
        '#text' => 'Preset Name',
        '#style' => ServiceLinksController::SERVICE_LINKS_STYLE_IMAGE_AND_TEXT,
        '#attributes' => [
          'class' => [
            'service-links-first',
          ],
          'title' => 'This is a link',
          'rel' => 'nofollow',
        ],
        '#image' => [
          '#theme' => 'image',
          '#uri' => '',
          '#alt' => 'Preset Name',
        ],
      ],
    ];

    $this->assertEquals($expected, $actual);
  }

  /**
   * renderOne returns adds target => blank if a new window is requested.
   *
   * @test
   */
  public function renderOneAddsTargetBlankIfNewWindowSet() {
    list($tokens, $cache_tags) = $this->serviceLinksController->getTokens(NULL);

    $node = $this->getMockEntity();
    list($services, $tags) = $this->serviceLinksController->getLinks($node, []);
    $cache_tags = array_merge($cache_tags, $tags);

    $actual = $this->serviceLinksController->renderOne('first', $services[0], $node, $tokens);
    $this->assertInstanceOf('Drupal\Core\Url', $actual['first']['#url']);
    unset($actual['first']['#url']);

    $expected = [
      'first' => [
        '#type' => 'service_link',
        '#attached' => [
          'library' => [],
          'drupalSettings' => [],
        ],
        '#text' => 'Preset Name',
        '#style' => ServiceLinksController::SERVICE_LINKS_STYLE_IMAGE_AND_TEXT,
        '#attributes' => [
          'class' => [
            'service-links-first',
          ],
          'title' => 'This is a link',
          'rel' => 'nofollow',
          'target' => '_blank',
        ],
        '#image' => [
          '#theme' => 'image',
          '#uri' => '',
          '#alt' => 'Preset Name',
        ],
      ],
    ];

    $this->assertEquals($expected, $actual);
  }

  /**
   * Build method returns an array of link render arrays.
   *
   * @test
   */
  public function buildReturnsArrayOfLinksAndCacheTags() {
    $entity = $this->getMockEntity();
    $context = [];

    list($links, $cache_tags) = $this->serviceLinksController->build($entity, $context);

    $this->assertInstanceOf('Drupal\Core\Url', $links['first']['#url']);
    unset($links['first']['#url']);

    $this->assertInstanceOf('Drupal\Core\Url', $links['second_1']['#url']);
    unset($links['second_1']['#url']);

    $this->assertInstanceOf('Drupal\Core\Url', $links['second_2']['#url']);
    unset($links['second_2']['#url']);

    $this->assertInstanceOf('Drupal\Core\Url', $links['second_3']['#url']);
    unset($links['second_3']['#url']);

    $expectedLinks = [
      'first' => [
        '#type' => 'service_link',
        '#attached' => [
          'library' => [],
          'drupalSettings' => [],
        ],
        '#text' => 'Preset Name',
        '#style' => ServiceLinksController::SERVICE_LINKS_STYLE_IMAGE_AND_TEXT,
        '#attributes' => [
          'class' => [
            'service-links-first',
          ],
          'title' => 'This is a link',
          'rel' => 'nofollow',
        ],
        '#image' => [
          '#theme' => 'image',
          '#uri' => '',
          '#alt' => 'Preset Name',
        ],
      ],
      'second_1' => [
        '#type' => 'service_link',
        '#attached' => [
          'library' => [],
          'drupalSettings' => [],
        ],
        '#text' => 'Second plugin',
        '#style' => ServiceLinksController::SERVICE_LINKS_STYLE_IMAGE_AND_TEXT,
        '#attributes' => [
          'class' => [
            'service-links-second-1',
          ],
          'title' => 'Foo=bar',
          'rel' => 'nofollow',
        ],
        '#image' => [
          '#theme' => 'image',
          '#uri' => '',
          '#alt' => 'Second plugin',
        ],
      ],
      'second_2' => [
        '#type' => 'service_link',
        '#attached' => [
          'library' => [],
          'drupalSettings' => [],
        ],
        '#text' => 'Second plugin',
        '#style' => ServiceLinksController::SERVICE_LINKS_STYLE_IMAGE_AND_TEXT,
        '#attributes' => [
          'class' => [
            'service-links-second-2',
          ],
          'title' => 'A front page link.',
          'rel' => 'nofollow',
        ],
        '#image' => [
          '#theme' => 'image',
          '#uri' => '',
          '#alt' => 'Second plugin',
        ],
      ],
      'second_3' => [
        '#type' => 'service_link',
        '#attached' => [
          'library' => [],
          'drupalSettings' => [],
        ],
        '#text' => 'Second plugin',
        '#style' => ServiceLinksController::SERVICE_LINKS_STYLE_IMAGE_AND_TEXT,
        '#attributes' => [
          'class' => [
            'service-links-second-3',
          ],
          'title' => 'A boring old link.',
          'rel' => 'nofollow',
        ],
        '#image' => [
          '#theme' => 'image',
          '#uri' => '',
          '#alt' => 'Second plugin',
        ],
      ],
    ];

    $this->assertEquals($expectedLinks, $links);
  }

  /**
   * Build method doesn't render entity if display is disabled.
   *
   * @test
   */
  public function buildDoesntRenderEntityIfDisplayDisabled() {
    $entity = $this->getMockEntity();
    $context = [];

    list($links, $cache_tags) = $this->serviceLinksController->build($entity, $context);

    $this->assertEquals([], $links);
  }

  /**
   * Build method doesn't render entity if the user lacks permission to see links.
   *
   * @test
   */
  public function buildDoesntRenderEntityIfNoPermission() {
    $entity = $this->getMockEntity();
    $context = [];

    list($links, $cache_tags) = $this->serviceLinksController->build($entity, $context);

    $this->assertEquals([], $links);
  }


  /**
   * fieldFormatterView method returns the expected render array of links.
   *
   * @test
   */
  public function fieldFormatterViewReturnsTheExpectedRenderArray() {
    $entity = $this->getMockEntity();
    $container = $this->getMockContainerEntity();
    $context = [];

    $links = $this->serviceLinksController->fieldFormatterView($entity, $container, 'myServiceLinksField');

    $this->assertInstanceOf('Drupal\Core\Url', $links['#links']['first']['#url']);
    unset($links['#links']['first']['#url']);

    $this->assertInstanceOf('Drupal\Core\Url', $links['#links']['second_1']['#url']);
    unset($links['#links']['second_1']['#url']);

    $this->assertInstanceOf('Drupal\Core\Url', $links['#links']['second_2']['#url']);
    unset($links['#links']['second_2']['#url']);

    $this->assertInstanceOf('Drupal\Core\Url', $links['#links']['second_3']['#url']);
    unset($links['#links']['second_3']['#url']);

    $expected = [
      '#type' => 'service_links',
      '#style' => 'default',
      '#links' => [
        'first' => [
          '#type' => 'service_link',
          '#attached' => [
            'library' => [],
            'drupalSettings' => [],
          ],
          '#text' => 'Preset Name',
          '#style' => ServiceLinksController::SERVICE_LINKS_STYLE_IMAGE_AND_TEXT,
          '#attributes' => [
            'class' => [
              'service-links-first',
            ],
            'title' => 'This is a link',
            'rel' => 'nofollow',
          ],
          '#image' => [
            '#theme' => 'image',
            '#uri' => '',
            '#alt' => 'Preset Name',
          ],
        ],
        'second_1' => [
          '#type' => 'service_link',
          '#attached' => [
            'library' => [],
            'drupalSettings' => [],
          ],
          '#text' => 'Second plugin',
          '#style' => ServiceLinksController::SERVICE_LINKS_STYLE_IMAGE_AND_TEXT,
          '#attributes' => [
            'class' => [
              'service-links-second-1',
            ],
            'title' => 'Foo=bar',
            'rel' => 'nofollow',
          ],
          '#image' => [
            '#theme' => 'image',
            '#uri' => '',
            '#alt' => 'Second plugin',
          ],
        ],
        'second_2' => [
          '#type' => 'service_link',
          '#attached' => [
            'library' => [],
            'drupalSettings' => [],
          ],
          '#text' => 'Second plugin',
          '#style' => ServiceLinksController::SERVICE_LINKS_STYLE_IMAGE_AND_TEXT,
          '#attributes' => [
            'class' => [
              'service-links-second-2',
            ],
            'title' => 'A front page link.',
            'rel' => 'nofollow',
          ],
          '#image' => [
            '#theme' => 'image',
            '#uri' => '',
            '#alt' => 'Second plugin',
          ],
        ],
        'second_3' => [
          '#type' => 'service_link',
          '#attached' => [
            'library' => [],
            'drupalSettings' => [],
          ],
          '#text' => 'Second plugin',
          '#style' => ServiceLinksController::SERVICE_LINKS_STYLE_IMAGE_AND_TEXT,
          '#attributes' => [
            'class' => [
              'service-links-second-3',
            ],
            'title' => 'A boring old link.',
            'rel' => 'nofollow',
          ],
          '#image' => [
            '#theme' => 'image',
            '#uri' => '',
            '#alt' => 'Second plugin',
          ],
        ],
      ],
      '#entity_type' => 'node',
      '#container_type' => 'my_block_type',
      '#container_field' => 'myServiceLinksField',
      '#cache' => [
        'tags' => [
          'user.permissions',
          'config:service_links.settings',
          'node:1',
          'config:system.site',
          'service_link_order:first,second_3,second_2,second_1',
        ]
        ],
      ];

    $this->assertEquals($expected, $links);
  }

}
