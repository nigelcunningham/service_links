<?php

namespace Drupal\Tests\service_links\Unit;

require_once __DIR__ . '/ServiceLinksControllerMockFunctions.php';

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\StringTranslation\TranslationManager;
use Drupal\service_links\Plugin\Field\FieldFormatter\ServiceLinksFieldFormatter;
use Drupal\service_links\Controller\ServiceLinksController;
use Drupal\Tests\UnitTestCase;

/**
 * Tests for the ServiceLinksFieldFormatter.
 *
 * @group service_links
 */
class ServiceLinksFieldFormatterTest extends UnitTestCase {

  /**
   * The Service Links field formatter instance.
   *
   * @var \Drupal\service_links\Plugin\Field\FieldFormatter\ServiceLinksFieldFormatter.
   */
  protected $serviceLinksFieldFormatter;

  /**
   * isApplicable matches service_links fields.
   *
   * @test
   */
  public function isApplicableMatchesServiceLinksFields() {
    $mockField = $this->getMockBuilder('Drupal\Core\Field\FieldDefinitionInterface')
      ->disableOriginalConstructor()
      ->getMock();

    $mockField->method('getSetting')
      ->with('target_type')
      ->willReturn('service_links');

    $this->assertTrue(ServiceLinksFieldFormatter::isApplicable($mockField));
  }

  /**
   * Formatter provides default settings.
   *
   * @test
   */
  public function formatterProvidesDefaultSettings() {
    $actual = ServiceLinksFieldFormatter::defaultSettings();

    $expected = [
      'theme_variant' => 'default',
      'link_style' => ServiceLinksController::SERVICE_LINKS_STYLE_IMAGE_AND_TEXT,
    ];

    $this->assertEquals($expected, $actual);
  }

  /**
   * Formatter settings summary defaults to list of links, link style 'unrecognised'.
   *
   * @test
   */
  public function settingsSummaryDefaultsToListAndUnrecognisedStyle() {
    parent::setUp();

    $container = new ContainerBuilder();

    $service = $this->createMock('Drupal\Core\StringTranslation\TranslationManager');

    $service->method('translate')
      ->willReturn('Translated');

    $container->set('string_translation', $service);
    \Drupal::setContainer($container);

    $plugin_id = 'plugin';
    $plugin_definition = false;

    $mockField = $this->getMockBuilder('Drupal\Core\Field\FieldDefinitionInterface')
      ->disableOriginalConstructor()
      ->getMock();

    $mockField->method('getSetting')
      ->with('target_type')
      ->willReturn('service_links');

    $label = [];
    $view_mode = '';
    $third_party_settings = [];

    $settings = [ ];

    $instance = new ServiceLinksFieldFormatter($plugin_id, $plugin_definition, $mockField, $settings, $label, $view_mode, $third_party_settings);

    $actual = $instance->settingsSummary();
    $actual[0] = $actual[0]->getUntranslatedString();
    $actual[1] = $actual[1]->getUntranslatedString();

    $expected = [
      'Theme variant: List of links',
      'Link style: Image and text'
    ];

    $this->assertEquals($expected, $actual);
  }

  /**
   * Formatter settings summary defaults to list of links, link style 'unrecognised'.
   *
   * @test
   */
  public function settingsSummaryHandlesFisheyeOptionCorrectly() {
    parent::setUp();

    $container = new ContainerBuilder();

    $service = $this->createMock('Drupal\Core\StringTranslation\TranslationManager');

    $service->method('translate')
      ->willReturn('Translated');

    $container->set('string_translation', $service);
    \Drupal::setContainer($container);

    $plugin_id = 'plugin';
    $plugin_definition = false;

    $mockField = $this->getMockBuilder('Drupal\Core\Field\FieldDefinitionInterface')
      ->disableOriginalConstructor()
      ->getMock();

    $mockField->method('getSetting')
      ->with('target_type')
      ->willReturn('service_links');

    $label = [];
    $view_mode = '';
    $third_party_settings = [];

    $settings = [
      'theme_variant' => 'fisheye',
    ];

    $instance = new ServiceLinksFieldFormatter($plugin_id, $plugin_definition, $mockField, $settings, $label, $view_mode, $third_party_settings);

    $actual = $instance->settingsSummary();
    $actual[0] = $actual[0]->getUntranslatedString();
    $actual[1] = $actual[1]->getUntranslatedString();

    $expected = [
      'Theme variant: Fisheye display',
      'Link style: Image and text'
    ];

    $this->assertEquals($expected, $actual);
  }

  /**
   * Formatter settings summary defaults to list of links, link style 'unrecognised'.
   *
   * @test
   */
  public function settingsSummaryHandlesTextOnlyOptionCorrectly() {
    parent::setUp();

    $container = new ContainerBuilder();

    $service = $this->createMock('Drupal\Core\StringTranslation\TranslationManager');

    $service->method('translate')
      ->willReturn('Translated');

    $container->set('string_translation', $service);
    \Drupal::setContainer($container);

    $plugin_id = 'plugin';
    $plugin_definition = false;

    $mockField = $this->getMockBuilder('Drupal\Core\Field\FieldDefinitionInterface')
      ->disableOriginalConstructor()
      ->getMock();

    $mockField->method('getSetting')
      ->with('target_type')
      ->willReturn('service_links');

    $label = [];
    $view_mode = '';
    $third_party_settings = [];

    $settings = [
      'link_style' => ServiceLinksController::SERVICE_LINKS_STYLE_TEXT,
    ];

    $instance = new ServiceLinksFieldFormatter($plugin_id, $plugin_definition, $mockField, $settings, $label, $view_mode, $third_party_settings);

    $actual = $instance->settingsSummary();
    $actual[0] = $actual[0]->getUntranslatedString();
    $actual[1] = $actual[1]->getUntranslatedString();

    $expected = [
      'Theme variant: List of links',
      'Link style: Text only'
    ];

    $this->assertEquals($expected, $actual);
  }

  /**
   * Formatter settings summary defaults to list of links, link style 'unrecognised'.
   *
   * @test
   */
  public function settingsSummaryHandlesImageOnlyOptionCorrectly() {
    parent::setUp();

    $container = new ContainerBuilder();

    $service = $this->createMock('Drupal\Core\StringTranslation\TranslationManager');

    $service->method('translate')
      ->willReturn('Translated');

    $container->set('string_translation', $service);
    \Drupal::setContainer($container);

    $plugin_id = 'plugin';
    $plugin_definition = false;

    $mockField = $this->getMockBuilder('Drupal\Core\Field\FieldDefinitionInterface')
      ->disableOriginalConstructor()
      ->getMock();

    $mockField->method('getSetting')
      ->with('target_type')
      ->willReturn('service_links');

    $label = [];
    $view_mode = '';
    $third_party_settings = [];

    $settings = [
      'link_style' => ServiceLinksController::SERVICE_LINKS_STYLE_IMAGE,
    ];

    $instance = new ServiceLinksFieldFormatter($plugin_id, $plugin_definition, $mockField, $settings, $label, $view_mode, $third_party_settings);

    $actual = $instance->settingsSummary();
    $actual[0] = $actual[0]->getUntranslatedString();
    $actual[1] = $actual[1]->getUntranslatedString();

    $expected = [
      'Theme variant: List of links',
      'Link style: Image only'
    ];

    $this->assertEquals($expected, $actual);
  }

  /**
   * Formatter settings summary defaults to list of links, link style 'unrecognised'.
   *
   * @test
   */
  public function settingsSummaryHandlesImageAndTextOptionCorrectly() {
    parent::setUp();

    $container = new ContainerBuilder();

    $service = $this->createMock('Drupal\Core\StringTranslation\TranslationManager');

    $service->method('translate')
      ->willReturn('Translated');

    $container->set('string_translation', $service);
    \Drupal::setContainer($container);

    $plugin_id = 'plugin';
    $plugin_definition = false;

    $mockField = $this->getMockBuilder('Drupal\Core\Field\FieldDefinitionInterface')
      ->disableOriginalConstructor()
      ->getMock();

    $mockField->method('getSetting')
      ->with('target_type')
      ->willReturn('service_links');

    $label = [];
    $view_mode = '';
    $third_party_settings = [];

    $settings = [
      'link_style' => ServiceLinksController::SERVICE_LINKS_STYLE_IMAGE_AND_TEXT,
    ];

    $instance = new ServiceLinksFieldFormatter($plugin_id, $plugin_definition, $mockField, $settings, $label, $view_mode, $third_party_settings);

    $actual = $instance->settingsSummary();
    $actual[0] = $actual[0]->getUntranslatedString();
    $actual[1] = $actual[1]->getUntranslatedString();

    $expected = [
      'Theme variant: List of links',
      'Link style: Image and text'
    ];

    $this->assertEquals($expected, $actual);
  }

  /**
   * Formatter settings summary defaults to list of links, link style 'unrecognised'.
   *
   * @test
   */
  public function settingsSummaryHandlesEmptyOptionCorrectly() {
    parent::setUp();

    $container = new ContainerBuilder();

    $service = $this->createMock('Drupal\Core\StringTranslation\TranslationManager');

    $service->method('translate')
      ->willReturn('Translated');

    $container->set('string_translation', $service);
    \Drupal::setContainer($container);

    $plugin_id = 'plugin';
    $plugin_definition = false;

    $mockField = $this->getMockBuilder('Drupal\Core\Field\FieldDefinitionInterface')
      ->disableOriginalConstructor()
      ->getMock();

    $mockField->method('getSetting')
      ->with('target_type')
      ->willReturn('service_links');

    $label = [];
    $view_mode = '';
    $third_party_settings = [];

    $settings = [
      'link_style' => ServiceLinksController::SERVICE_LINKS_STYLE_EMPTY,
    ];

    $instance = new ServiceLinksFieldFormatter($plugin_id, $plugin_definition, $mockField, $settings, $label, $view_mode, $third_party_settings);

    $actual = $instance->settingsSummary();
    $actual[0] = $actual[0]->getUntranslatedString();
    $actual[1] = $actual[1]->getUntranslatedString();

    $expected = [
      'Theme variant: List of links',
      'Link style: Empty (Javascript provided)'
    ];

    $this->assertEquals($expected, $actual);
  }

  /**
   * Formatter settings summary defaults to list of links, link style 'unrecognised'.
   *
   * @test
   */
  public function settingsSummaryHandlesFisheyeLinkStyleOptionCorrectly() {
    parent::setUp();

    $container = new ContainerBuilder();

    $service = $this->createMock('Drupal\Core\StringTranslation\TranslationManager');

    $service->method('translate')
      ->willReturn('Translated');

    $container->set('string_translation', $service);
    \Drupal::setContainer($container);

    $plugin_id = 'plugin';
    $plugin_definition = false;

    $mockField = $this->getMockBuilder('Drupal\Core\Field\FieldDefinitionInterface')
      ->disableOriginalConstructor()
      ->getMock();

    $mockField->method('getSetting')
      ->with('target_type')
      ->willReturn('service_links');

    $label = [];
    $view_mode = '';
    $third_party_settings = [];

    $settings = [
      'link_style' => ServiceLinksController::SERVICE_LINKS_STYLE_FISHEYE,
    ];

    $instance = new ServiceLinksFieldFormatter($plugin_id, $plugin_definition, $mockField, $settings, $label, $view_mode, $third_party_settings);

    $actual = $instance->settingsSummary();
    $actual[0] = $actual[0]->getUntranslatedString();
    $actual[1] = $actual[1]->getUntranslatedString();

    $expected = [
      'Theme variant: List of links',
      'Link style: Fisheye optimised'
    ];

    $this->assertEquals($expected, $actual);
  }

}
