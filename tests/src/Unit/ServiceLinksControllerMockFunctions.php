<?php

/**
 * @file
 * ServiceLinksControllerMockFunctions.php.
 *
 * Mock functions for the Drupal\service_links\Controller namespace.
 */

namespace Drupal\service_links\Controller;

/**
 * Perform token replacements on a string.
 *
 * @param string $pattern
 *   The pattern to which replacements should be applied.
 * @param array $replacements
 *   The array supplying replacement content.
 *
 * @return string
 *   The pattern after replacements are made.
 */
function token_replace($pattern, array $replacements) {
  return 'Tokens Replaced';
}

/**
 * Mock shorten_url function.
 *
 * @param string $url
 *   The URL to be shortened.
 *
 * @return string
 *   The shortened version of the URL.
 */
function shorten_url($url) {
  return 'https://shortened.url?dest=' . $url;
}

/**
 * Mock t().
 *
 * @param string $text
 *   The text to be translated.
 * @param array $args
 *   The arguments to apply to the string.
 * @param array $options
 *   The array of options.
 *
 * @return string
 *   The 'translated' string.
 */
function t($text, array $args = [], array $options = []) {
  return $text;
}
