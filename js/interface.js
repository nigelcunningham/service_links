/**
 * Interface Elements for jQuery
 * Fisheye menu
 * 
 * http://interface.eyecon.ro
 * 
 * Copyright (c) 2006 Stefan Petre
 * Dual licensed under the MIT (MIT-LICENSE.txt) 
 * and GPL (GPL-LICENSE.txt) licenses.
 *
 */

/**
 * Build a Fisheye menu from a list of links
 *
 * @name Fisheye
 * @description Build a Fisheye menu from a list of links
 * @param Hash hash A hash of parameters
 * @option String items items selection
 * @option String container container element
 * @option Integer itemWidth the minimum width for each item
 * @option Integer maxWidth the maximum width for each item
 * @option String itemsText selection of element that contains the text for each item
 * @option Integer proximity the distance from element that make item to interact
 * @option String valign vertical alignment
 * @option String halign horizontal alignment
 *
 * @type jQuery
 * @cat Plugins/Interface
 * @author Stefan Petre
 *
 * Updated to provide itemHeight, proximityX and Y args by "Matt".
 * https://stackoverflow.com/questions/1806369/cant-change-this-javascript-to-read-less-for-height-than-width-on-mousemove
 *
 * Updated for removal of jQuery size() function by Nigel Cunningham <nigel@grindstone.com.au>
 */
jQuery.iFisheye = {

 build : function(options)
 {

  return this.each(
   function()
   {
    var el = this;
    el.fisheyeCfg = {
     items : jQuery(options.items, this),
     container: jQuery(options.container, this),
     pos : jQuery(this).offset(),
     itemWidth: options.itemWidth,
     itemHeight: options.itemHeight,
     itemsText: options.itemsText,
     proximityX: options.proximityX,
     proximityY: options.proximityY,
     valign: options.valign,
     halign: options.halign,
     maxWidth : options.maxWidth
    };
    jQuery.iFisheye.positionContainer(el, 0);
    jQuery(window).bind(
     'resize',
     function()
     {
      el.fisheyeCfg.pos = jQuery(el).offset();
      jQuery.iFisheye.positionContainer(el, 0);
      jQuery.iFisheye.positionItems(el);
     }
    );
    jQuery.iFisheye.positionItems(el);
    el.fisheyeCfg.items
     .bind(
      'mouseover',
      function()
      {
      var text = jQuery(el.fisheyeCfg.itemsText, this);
      if (text.length) {
        text.get(0).style.display = 'block';
      }
      }
     )
     .bind(
      'mouseout',
      function()
      {
        var text = jQuery(el.fisheyeCfg.itemsText, this);
        if (text.length) {
          text.get(0).style.display = 'none';
        }
      }
     );
    jQuery(document).bind(
     'mousemove',
     function(e)
     {
      var toAdd = 0;
      el.fisheyeCfg.pos = jQuery(el).offset();
      if (el.fisheyeCfg.halign && el.fisheyeCfg.halign == 'center')
       var posx = e.pageX - el.fisheyeCfg.pos.left - (el.offsetWidth - el.fisheyeCfg.itemWidth * el.fisheyeCfg.items.length)/2 - el.fisheyeCfg.itemWidth/2;
      else if (el.fisheyeCfg.halign && el.fisheyeCfg.halign == 'right')
       var posx = e.pageX - el.fisheyeCfg.pos.left - el.offsetWidth + el.fisheyeCfg.itemWidth * el.fisheyeCfg.items.length;
      else
       var posx = e.pageX - el.fisheyeCfg.pos.left;
      var posy = Math.pow(e.pageY - el.fisheyeCfg.pos.top - el.offsetHeight + el.fisheyeCfg.itemHeight,2);
      el.fisheyeCfg.items.each(
       function(nr)
       {
        distanceX = Math.sqrt(
         Math.pow(posx - nr*el.fisheyeCfg.itemWidth, 2)
        );
        distanceY = Math.sqrt(posy) - el.fisheyeCfg.itemHeight;
        distanceX -= el.fisheyeCfg.itemWidth/2;
        distanceX = distanceX < 0 ? 0 : distanceX;
        distanceX = distanceX > el.fisheyeCfg.proximityX ? el.fisheyeCfg.proximityX : distanceX;
        distanceX = el.fisheyeCfg.proximityX - distanceX;
        distanceY = distanceY > el.fisheyeCfg.proximityY ? el.fisheyeCfg.proximityY : distanceY;
        distanceY = el.fisheyeCfg.proximityY - distanceY;
        extraWidth = el.fisheyeCfg.maxWidth/4 * (distanceX*distanceY)/(el.fisheyeCfg.proximityX*el.fisheyeCfg.proximityY); // divided by 4 to smooth the sizing transition
        extraWidth = (extraWidth > el.fisheyeCfg.maxWidth) ? el.fisheyeCfg.maxWidth : extraWidth;
        this.style.width = el.fisheyeCfg.itemWidth + extraWidth + 'px';
        this.style.left = el.fisheyeCfg.itemWidth * nr + toAdd + 'px';
        toAdd += extraWidth;
       }
      );
      jQuery.iFisheye.positionContainer(el, toAdd);
     }
    );
   }
  )
 },

 positionContainer : function(el, toAdd)
 {
  if (el.fisheyeCfg.halign)
   if (el.fisheyeCfg.halign == 'center')
    el.fisheyeCfg.container.get(0).style.left = (el.offsetWidth - el.fisheyeCfg.itemWidth * el.fisheyeCfg.items.length)/2 - toAdd/2 + 'px';
   else if (el.fisheyeCfg.halign == 'left')
    el.fisheyeCfg.container.get(0).style.left =  - toAdd/el.fisheyeCfg.items.length + 'px';
   else if (el.fisheyeCfg.halign == 'right')
    el.fisheyeCfg.container.get(0).style.left =  (el.offsetWidth - el.fisheyeCfg.itemWidth * el.fisheyeCfg.items.length) - toAdd/2 + 'px';
  el.fisheyeCfg.container.get(0).style.width = el.fisheyeCfg.itemWidth * el.fisheyeCfg.items.length + toAdd + 'px';
 },

 positionItems : function(el)
 {
  el.fisheyeCfg.items.each(
   function(nr)
   {
    this.style.width = el.fisheyeCfg.itemWidth + 'px';
    this.style.left = el.fisheyeCfg.itemWidth * nr + 'px';
   }
  );
 }
};

jQuery.fn.Fisheye = jQuery.iFisheye.build;
