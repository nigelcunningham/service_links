<?php

namespace Drupal\widget_services\Plugin\ServiceLink;

use Drupal\service_links\ServiceLinkBase;
use Drupal\service_links\Controller\ServiceLinksController;
use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\Entity;

/**
 * Class WidgetServices.
 *
 * @ServiceLink(
 *   id = "widget_services",
 *   name = @Translation("Widget Services"),
 *   service_links_config_form = "Drupal\widget_services\Form\ServiceLinksWidgetConfigurationForm"
 * )
 */
class WidgetServices extends ServiceLinkBase {

  /**
   * {@inheritdoc}
   */
  public function getDetails() {
    return [
      'facebook_share' => [
        'name' => t('Share on Facebook'),
        'link' => '//www.facebook.com/sharer.php',
        'description' => t('Share this post on Facebook'),
        'library' => 'widget_services/facebook_share',
        'icon' => 'facebook.png',
        'style' => ServiceLinksController::SERVICE_LINKS_STYLE_EMPTY,
        'callback' => 'wsFacebookShareData',
      ],
      'facebook_like' => [
        'name' => 'Facebook Like',
        'link' => '//www.facebook.com/plugins/like.php?href=<encoded-url>&layout=<layout>&show_faces=<show_faces>&action=<action>&colorscheme=<colorscheme>&width=<width>&height=<height>&font=<font>&locale=<locale>',
        'icon' => 'facebook.png',
        'description' => t('I Like it'),
        'library' => 'widget_services/facebook_like',
        'style' => ServiceLinksController::SERVICE_LINKS_STYLE_EMPTY,
        'callback' => 'wsFacebookLikeData',
      ],
      'google_plus_one' => [
        'name' => 'Google Plus One',
        'link' => '<url>',
        'description' => t('Plus it'),
        'library' => 'widget_services/google_plus_one',
        'style' => ServiceLinksController::SERVICE_LINKS_STYLE_EMPTY,
        'callback' => 'wsGooglePlusOneData',
      ],
      'linkedin_share_button' => [
        'name' => 'Linkedin Share Button',
        'link' => '<url>',
        'icon' => 'linkedin.png',
        'description' => t('Share on Linkedin'),
        'library' => 'widget_services/linkedin',
        'style' => ServiceLinksController::SERVICE_LINKS_STYLE_EMPTY,
        'callback' => 'wsLinkedInShareButtonData',
      ],
      'pinterest_button' => [
        'name' => 'Pinterest',
        'link' => '//pinterest.com/pin/create/button/?url=<raw-encoded-long-url>&description=<pinterest-description>&media=<pinterest-media>',
        'icon' => 'pinterest.png',
        'description' => t('Pin It'),
        'library' => 'widget_services/pinterest_button',
        'style' => ServiceLinksController::SERVICE_LINKS_STYLE_EMPTY,
        'attributes' => ['class' => ['pin-it-button']],
        'callback' => 'wsPinterestButtonData',
        'preset' => 'wsPinterestButtonTags',
      ],
    ];
  }

  /**
   * Callback function for Twitter Widget.
   *
   * @param \stdClass $service
   *   The service info.
   * @param array $subst
   *   The token substitutions.
   *
   * @return \stdClass
   *   The updated service information.
   */
  public function wsTwitterWidgetData(\stdClass $service, array $subst) {

    $config = \Drupal::service('config.factory')->getEditable('widget_services.settings');

    $params = [
      'via' => Html::escape($config->get('service_links_tw_data_via')),
    ];

    $service->url[1] = array_merge($service->url[1], $params);
    return $service;
  }

  /**
   * Callback function for Facebook Share.
   *
   * @param \stdClass $service
   *   The service info.
   * @param array $subst
   *   The token substitutions.
   *
   * @return \stdClass
   *   The updated service information.
   */
  public function wsFacebookShareData(\stdClass $service, array $subst) {

    $config = \Drupal::service('config.factory')->getEditable('widget_services.settings');

    $fs_settings = [
      'type' => Html::escape($config->get('service_links_fs_type')),
      'app_id' => Html::escape($config->get('service_links_fs_app_id')),
      'css' => Html::escape(preg_replace('/[^0-9a-z\-:;]/', '', $config->get('service_links_fs_css'))),
    ];

    $service->detail['drupalSettings']['ws_fs'] = $fs_settings;

    $service->attributes['rel'] = $subst['url'];
    return $service;
  }

  /**
   * Callback function for Facebook Like.
   *
   * @param \stdClass $service
   *   The service info.
   * @param array $subst
   *   The token substitutions.
   *
   * @return \stdClass
   *   The updated service information.
   */
  public function wsFacebookLikeData(\stdClass $service, array $subst) {
    $config = \Drupal::service('config.factory')->getEditable('widget_services.settings');

    $params = [
      'layout' => Html::escape($config->get('service_links_fl_layout')),
      'show_faces' => Html::escape($config->get('service_links_fl_show_faces')),
      'action' => Html::escape($config->get('service_links_fl_action')),
      'colorscheme' => Html::escape($config->get('service_links_fl_colorscheme')),
      'font' => Html::escape($config->get('service_links_fl_font')),
      'width' => (int) $config->get('service_links_fl_width'),
      'height' => (int) $config->get('service_links_fl_height'),
      'locale' => Html::escape($config->get('service_links_fl_locale')),
    ];

    $fl_settings = [
      'width' => $params['width'],
      'height' => $params['height'],
    ];

    $service->detail['drupalSettings']['ws_fl'] = $fl_settings;

    $service->url[1] = array_merge($service->url[1], $params);
    return $service;
  }

  /**
   * Callback function for Google Plus One.
   *
   * @param \stdClass $service
   *   The service info.
   * @param array $subst
   *   The token substitutions.
   *
   * @return \stdClass
   *   The updated service information.
   */
  public function wsGooglePlusOneData(\stdClass $service, array $subst) {
    $config = \Drupal::service('config.factory')->getEditable('widget_services.settings');

    $gpo_settings = [
      'size' => Html::escape($config->get('service_links_gpo_size')),
      'annotation' => Html::escape($config->get('service_links_gpo_annotation')),
      'lang' => Html::escape($config->get('service_links_gpo_lang')),
      'callback' => Html::escape($config->get('service_links_gpo_callback')),
      'width' => (int) $config->get('service_links_gpo_width'),
    ];

    $service->detail['drupalSettings']['ws_gpo'] = $gpo_settings;
    return $service;
  }

  /**
   * Callback function for Linkedin Share Button.
   *
   * @param \stdClass $service
   *   The service info.
   * @param array $subst
   *   The token substitutions.
   *
   * @return \stdClass
   *   The updated service information.
   */
  public function wsLinkedInShareButtonData(\stdClass $service, array $subst) {
    $config = \Drupal::service('config.factory')->getEditable('widget_services.settings');

    $lsb_settings = [
      'countmode' => Html::escape($config->get('service_links_lsb_countmode')),
    ];

    $service->detail['drupalSettings']['ws_lsb'] = $lsb_settings;

    return $service;
  }

  /**
   * Preset function for Pinterest Button, which fill the media tag.
   *
   * @param \stdClass $service
   *   The service info.
   * @param array $settings
   *   The settings array.
   * @param \Drupal\Core\Entity\Entity|null $entity
   *   The entity being displayed.
   *
   * @return array
   *   The updated service class and settings array.
   */
  public function wsPinterestButtonTags(\stdClass $service, array $tokens, Entity $entity = NULL) {
    $config = \Drupal::service('config.factory')->getEditable('widget_services.settings');

    $extraTokens['media'] = $config->get('service_links_pb_mediatoken');
    $extraTokens['description'] = $config->get('service_links_pb_descriptiontoken');

    $tokens['tag']['pinterest-media'] = '<pinterest-media>';

    $tokens['subst']['pinterest-media'] = Html::escape(\Drupal::token()->replace($extraTokens['media'], ['entity' => $entity]));
    if (empty($extraTokens['description'])) {
      $service->detail['link'] = str_replace('pinterest-description', 'raw-encoded-teaser', $service->detail['link']);
    }
    else {
      $tokens['tag']['pinterest-description'] = '<pinterest-description>';
      $tokens['subst']['pinterest-description'] = Html::escape(\Drupal::token()->replace($extraTokens['description'], ['entity' => $entity]));
    }

    return [$service, $tokens];
  }

  /**
   * Callback function for Pinterest Button.
   *
   * @param \stdClass $service
   *   Unused service info.
   * @param array $subst
   *   Unused substitutions for the URL.
   *
   * @return \stdClass
   *   The updated service information.
   */
  public function wsPinterestButtonData(\stdClass $service, array $subst) {
    $config = \Drupal::service('config.factory')->getEditable('widget_services.settings');

    $pb_settings = [
      'countlayout' => Html::escape($config->get('service_links_pb_countlayout')),
    ];

    $service->detail['drupalSettings']['ws_pb'] = $pb_settings;

    return $service;
  }

}
