<?php

namespace Drupal\widget_services\FormAlterations;

use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Extension\ModuleHandler;

/**
 * Provides the form alter hooks for the service links configuration forms.
 */
class ServiceLinksWidgetConfigurationForm  {

  /**
   * @param array &$form
   *   The form render array being altered.
   */
  public static function alter(array &$form, FormStateInterface $form_state) {

    $config = \Drupal::configFactory()->getEditable($form['#settingsKey']);

    $form['twitter_widget'] = [
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#title' => t('Twitter Widget'),
    ];
    $form['twitter_widget']['service_links_tw_data_via'] = [
      '#type' => 'textfield',
      '#field_prefix' => '@',
      '#title' => t('Via user'),
      '#description' => t('Add here your screen name.'),
      '#default_value' => $config->get('service_links_tw_data_via'),
      '#size' => 40,
    ];

    $form['facebook_like'] = [
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#title' => t('Facebook Like'),
    ];
    $form['facebook_like']['service_links_fl_layout'] = [
      '#type' => 'select',
      '#title' => t('Layout'),
      '#default_value' => $config->get('service_links_fl_layout'),
      '#options' => [
        'standard' => t('Standard'),
        'button_count' => t('Button Count'),
        'box_count' => t('Box Count'),
      ],
    ];
    $form['facebook_like']['service_links_fl_width'] = [
      '#title' => t('Width'),
      '#type' => 'textfield',
      '#default_value' => $config->get('service_links_fl_width'),
      '#description' => t('A good value for button layout is 100, for box layout is 80 and for standard layout 450'),
      '#size' => 10,
    ];
    $form['facebook_like']['service_links_fl_height'] = [
      '#title' => t('Height'),
      '#type' => 'textfield',
      '#default_value' => $config->get('service_links_fl_height'),
      '#description' => t('A good value for button layout is 21, for box layout is 65 and for standard layout is 80'),
      '#size' => 10,
    ];
    $form['facebook_like']['service_links_fl_font'] = [
      '#title' => t('Font'),
      '#type' => 'select',
      '#default_value' => $config->get('service_links_fl_font'),
      '#options' => [
        '' => t('None'),
        'arial' => t('Arial'),
        'lucida grande' => t('Lucida Grande'),
        'segoe ui' => t('Segoe Ui'),
        'tahoma' => t('Tahoma'),
        'trebuchet ms' => t('Trebuchet Ms'),
        'verdana' => t('Verdana'),
      ],
    ];
    $form['facebook_like']['service_links_fl_show_faces'] = [
      '#title' => t('Show Faces'),
      '#type' => 'radios',
      '#default_value' => $config->get('service_links_fl_show_faces'),
      '#options' => [
        'true' => t('Yes'),
        'false' => t('No'),
      ],
      '#attributes' => ['class' => ['container-inline']],
      '#description' => t('This works only when the standard layout is set on'),
    ];
    $form['facebook_like']['service_links_fl_colorscheme'] = [
      '#title' => t('Color Scheme'),
      '#type' => 'radios',
      '#default_value' => $config->get('service_links_fl_colorscheme'),
      '#options' => [
        'light' => t('Light'),
        'dark' => t('Dark'),
      ],
      '#attributes' => ['class' => ['container-inline']],
    ];
    $form['facebook_like']['service_links_fl_action'] = [
      '#title' => t('Action'),
      '#type' => 'radios',
      '#default_value' => $config->get('service_links_fl_action'),
      '#options' => [
        'like' => t('Like'),
        'recommend' => t('Recommend'),
      ],
      '#attributes' => ['class' => ['container-inline']],
    ];
    $form['facebook_like']['service_links_fl_locale'] = [
      '#title' => t('Locale'),
      '#type' => 'textfield',
      '#default_value' => $config->get('service_links_fl_locale'),
      '#description' => t('Enter the language code and the country code separated by an underscore (i.e. es_ES, en_GB, en_US, it_IT, fr_FR, pt_BR, pt_PT)'),
      '#size' => 5,
      '#attributes' => ['class' => ['container-inline']],
    ];

    $form['facebook_share'] = [
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#title' => t('Facebook Share'),
    ];
    $form['facebook_share']['service_links_fs_type'] = [
      '#type' => 'select',
      '#title' => t('Type'),
      '#default_value' => $config->get('service_links_fs_type'),
      '#options' => [
        'button' => t('Button'),
        'button_count' => t('Button Count'),
        'box_count' => t('Box Count'),
        'icon_link' => t('Icon Link'),
      ],
    ];
    $form['facebook_share']['service_links_fs_app_id'] = [
      '#type' => 'textfield',
      '#title' => t('App ID'),
      '#default_value' => $config->get('service_links_fs_app_id'),
      '#description' => t('You may generate your app ID at the <a href=@url>Facebook apps</a> page', ['@url' => 'https://developers.facebook.com/apps']),
      '#size' => 40,
    ];
    $form['facebook_share']['service_links_fs_css'] = [
      '#type' => 'textfield',
      '#title' => t('Style'),
      '#default_value' => $config->get('service_links_fs_css'),
      '#description' => t('Apply a CSS style providing a single string (i.e. %style)', ['%style' => 'text-align:left;vertical-align:top;margin-top:4px']),
      '#size' => 80,
    ];

    $form['google_plus_one'] = [
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#title' => t('Google Plus One'),
    ];
    $form['google_plus_one']['service_links_gpo_size'] = [
      '#title' => t('Size'),
      '#type' => 'select',
      '#default_value' => $config->get('service_links_gpo_size'),
      '#options' => [
        '' => t('Standard'),
        'small' => t('Small'),
        'medium' => t('Medium'),
        'tall' => t('Tall'),
      ],
    ];
    $form['google_plus_one']['service_links_gpo_annotation'] = [
      '#title' => t('Annotation'),
      '#type' => 'select',
      '#default_value' => $config->get('service_links_gpo_annotation'),
      '#options' => [
        '' => t('Bubble'),
        'none' => t('None'),
        'inline' => t('Inline'),
      ],
    ];
    $form['google_plus_one']['service_links_gpo_width'] = [
      '#title' => t('Max width'),
      '#type' => 'textfield',
      '#default_value' => $config->get('service_links_gpo_width'),
      '#description' => t('If annotation is set to "inline", this parameter sets the width in pixels to use for the button and its inline annotation. If the width is omitted, a button and its inline annotation use 450px. A minimum value of 120 is required.
See <a href="@url">Inline annotation widths</a> for examples of how the annotation is displayed for various width settings', ['@url' => 'https://developers.google.com/+/web/+1button/?hl=it#inline-annotation']),
      '#size' => 10,
    ];
    $form['google_plus_one']['service_links_gpo_lang'] = [
      '#title' => t('Language'),
      '#type' => 'textfield',
      '#default_value' => $config->get('service_links_gpo_lang'),
      '#description' => t('Sets the language to use for the +1 buttons on the page when annotation is set to "inline". For available language code values, see the <a href="@url">list of supported language codes</a>', ['@url' => 'https://developers.google.com/+/web/+1button/?hl=it#available-languages']),
      '#size' => 5,
    ];
    $form['google_plus_one']['service_links_gpo_callback'] = [
      '#title' => t('Callback function'),
      '#type' => 'textfield',
      '#default_value' => $config->get('service_links_gpo_callback'),
      '#description' => t('If specified, this function is called after the user clicks the +1 button.'),
      '#size' => 40,
    ];

    $form['linkedin_share_button'] = [
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#title' => t('Linkedin Share Button'),
    ];
    $form['linkedin_share_button']['service_links_lsb_countmode'] = [
      '#title' => t('Count Mode'),
      '#type' => 'select',
      '#default_value' => $config->get('service_links_lsb_countmode'),
      '#options' => [
        'top' => t('Vertical'),
        'right' => t('Horizontal'),
        '' => t('No Count'),
      ],
    ];

    $form['pinterest_button'] = [
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#title' => t('Pinterest Button'),
    ];
    $form['pinterest_button']['service_links_pb_countlayout'] = [
      '#title' => t('Pin Count'),
      '#type' => 'select',
      '#default_value' => $config->get('service_links_pb_countlayout'),
      '#options' => [
        'horizontal' => t('Horizontal'),
        'vertical' => t('Vertical'),
        'none' => t('No Count'),
      ],
    ];
    $form['pinterest_button']['service_links_pb_mediatoken'] = [
      '#title' => t('Media parameter'),
      '#type' => 'textfield',
      '#default_value' => $config->get('service_links_pb_mediatoken'),
      '#description' => t('Fill the media parameter with a token which will be evaluated by <a href=@url>Token</a> module.', ['@url' => 'http://www.drupal.org/project/token']),
      '#size' => 40,
    ];
    $form['pinterest_button']['service_links_pb_descriptiontoken'] = [
      '#title' => t('Description parameter'),
      '#type' => 'textfield',
      '#default_value' => $config->get('service_links_pb_descriptiontoken'),
      '#description' => t('Fill the description parameter with a token which will be evaluated by <a href=@url>Token</a> module, if left blank will be replaced by the node teaser.', ['@url' => 'http://www.drupal.org/project/token']),
      '#size' => 40,
    ];

    $form['pinterest_button']['token_help'] = [
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#title' => t('Token help'),
      '#theme' => 'token_tree_link',
      '#token_types' => ['node'],
    ];

    $form['#submit'][] = ['\Drupal\widget_services\FormAlterations\ServiceLinksWidgetConfigurationForm', 'submit'];
  }

  /**
   * {@inheritdoc}
   */
  public static function submit(array &$form, FormStateInterface $form_state) {
    $settingsKey = $form['#settingsKey'];

    if (is_null($settingsKey)) {
      $settingsKey = 'service_links';
    }

    $config = \Drupal::configFactory()->getEditable($settingsKey);
    $fields = [
      'service_links_tw_data_via',
      'service_links_fl_layout',
      'service_links_fl_width',
      'service_links_fl_height',
      'service_links_fl_font',
      'service_links_fl_show_faces',
      'service_links_fl_colorscheme',
      'service_links_fl_action',
      'service_links_fl_locale',
      'service_links_fs_type',
      'service_links_fs_app_id',
      'service_links_fs_css',
      'service_links_gpo_size',
      'service_links_gpo_annotation',
      'service_links_gpo_width',
      'service_links_gpo_lang',
      'service_links_gpo_callback',
      'service_links_lsb_countmode',
      'service_links_pb_countlayout',
      'service_links_pb_mediatoken',
      'service_links_pb_descriptiontoken',
    ];

    if (\Drupal::moduleHandler()->moduleExists('token')) {
      $fields[] = 'pinterest_button';
    }

    foreach ($fields as $field) {
      $config->set($field, $form_state->getValue($field));
    }

    $config->save();
  }
}
