(function ($) {
   Drupal.behaviors.ws_gpo = {
    scriptadded: false,

    attach: function (context, settings) {
      $('a.service-links-google-plus-one', context).once('google-plus-one').each(function(){
        var g_text = document.createElement('g:plusone');

        g_text.setAttribute('href', $(this).attr('href'));
        g_text.setAttribute('width', drupalSettings.ws_gpo.width);

        if (drupalSettings.ws_gpo.size != '') {
          g_text.setAttribute('size', drupalSettings.ws_gpo.size);
        }
        if (drupalSettings.ws_gpo.annotation != '') {
          g_text.setAttribute('annotation', drupalSettings.ws_gpo.annotation);
        }
        if (drupalSettings.ws_gpo.callback) {
          g_text.setAttribute('callback', drupalSettings.ws_gpo.callback);
        }

        $(this).replaceWith(g_text);
      });

      if (this.scriptadded) {
        gapi.plusone.go();
      } else {
        var params = { parsetags: "explicit" };

        if (drupalSettings.ws_gpo.lang != '') {
          params.lang = drupalSettings.ws_gpo.lang;
        }

        window.___gcfg = params

        $.ajax({
          url: "https://apis.google.com/js/plusone.js",
          dataType: "script",
          cache: true,
          success: function () {
            this.scriptadded = true;
            gapi.plusone.go();
          }
        });
      }
    }  
  }
})(jQuery);
