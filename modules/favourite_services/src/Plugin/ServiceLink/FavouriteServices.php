<?php

namespace Drupal\favourite_services\Plugin\ServiceLink;

use Drupal\service_links\ServiceLinkBase;

/**
 * Class FavouriteServices.
 *
 * @ServiceLink(
 *   id = "favourite_services",
 *   name = @Translation("Favourite Services")
 * )
 */
class FavouriteServices extends ServiceLinkBase {

  /**
   * {@inheritdoc}
   */
  public function getDetails() {
    return [
      'favourite' => [
        'name' => t('Favourite'),
        'description' => t('Add this page in your favourites'),
        'link' => '<url>&favtitle=<raw-encoded-title>',
        'attributes' => ['style' => 'display:none;'],
        'library' => 'favourite_services/favourite_services',
      ],
    ];
  }

}
