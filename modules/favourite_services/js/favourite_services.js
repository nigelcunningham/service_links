(function ($) {
  Drupal.behaviors.sl_fs = {
    attach: function(context, settings) {
      var $favourites = $("a.service-links-favourite", context).once('service-links-favourite');
      var message = '';

      $favourites.show();
      if (window.chrome) {
        message= Drupal.t('Use CTRL + D to add this to your bookmarks');
      } else if (window.opera && window.print) {
        $favourites.each(function(){
          var url = $(this).attr('href').split('&favtitle=');
          var title = decodeURI(url[1]);
          url = url[0];
          $(this).attr('rel', 'sidebar').attr('href', url).attr('title', title);
        });
      } else if (window.sidebar || window.external) {
        $favourites.click(function(event){
          event.preventDefault();
          var url = $(this).attr('href').split('&favtitle=');
          var title = decodeURI(url[1]);
          url = url[0];
          if (window.sidebar) {
            window.sidebar.addPanel(title, url, '');
          } else if (window.external) {
            window.external.AddFavorite(url, title);
          }
        });
      } else {
        message = Drupal.t('Please use your browser to bookmark this page.');
      }
      if (message) {
        $favourites.click(function(event){
          event.preventDefault();
          alert(message);
        });
      }
    }
  }
})(jQuery);
