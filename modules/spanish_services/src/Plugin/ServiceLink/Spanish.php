<?php

namespace Drupal\spanish_services\Plugin\ServiceLink;

use Drupal\service_links\ServiceLinkBase;

/**
 * Class Spanish.
 *
 * @ServiceLink(
 *   id = "spanish",
 *   name = @Translation("Spanish")
 * )
 */
class Spanish extends ServiceLinkBase {

  /**
   * {@inheritdoc}
   */
  public function getDetails() {
    return [
      '_es_meneame' => [
        'name' => 'Meneame',
        'description' => t('Add to Meneame'),
        'link' => '//www.meneame.net/submit.php?url=<encoded-url>',
      ],
      '_es_barrapunto' => [
        'name' => 'Barrapunto',
        'description' => t('Publish this post on Barrapunto.com'),
        'link' => 'http://barrapunto.com/submit.pl?story=He leido en <encoded-source> el articulo <a href="<encoded-url>"><encoded-title></a>&subj=<encoded-title>',
      ],
    ];
  }

}
