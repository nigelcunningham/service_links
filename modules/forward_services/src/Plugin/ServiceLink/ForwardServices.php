<?php

namespace Drupal\forward_services\Plugin\ServiceLink;

use Drupal\service_links\ServiceLinkBase;
use Drupal\Core\Entity\Entity;

/**
 * Class ForwardServices.
 *
 * @ServiceLink(
 *   id = "forward_services",
 *   name = @Translation("Forward services")
 * )
 */
class ForwardServices extends ServiceLinkBase {

  /**
   * {@inheritdoc}
   */
  public function getDetails() {
    return [
      'forward' => [
        'name' => 'Forward',
        'link' => '<front-page>forward/<entity-type>/<entity-id>',
        'description' => t('Send to a friend'),
        'access' => 'accessCheck',
      ],
    ];
  }

  /**
   * Check whether to display the link.
   */
  public function accessCheck(Entity $entity = NULL) {
    $cache_tags = [];

    if (!$entity) {
      return [FALSE, $cache_tags];
    }

    $user = \Drupal::currentUser();
    $cache_tags[] = "user:{$user->id()}";

    if (!$user->hasPermission('access forward')) {
      return [FALSE, $cache_tags];
    }

    $forward_settings = \Drupal::config('forward.settings')->get();
    $cache_tags[] = "config:forward.settings";

    return [\Drupal::service('forward.access_checker')
      ->isAllowed($forward_settings, $entity),
      $cache_tags];
  }

}
