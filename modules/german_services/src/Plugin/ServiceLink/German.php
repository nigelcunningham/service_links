<?php

namespace Drupal\german_services\Plugin\ServiceLink;

use Drupal\service_links\ServiceLinkBase;

/**
 * Class German.
 *
 * @ServiceLink(
 *   id = "german",
 *   name = @Translation("German")
 * )
 */
class German extends ServiceLinkBase {

  /**
   * {@inheritdoc}
   */
  public function getDetails() {
    return [
      '_de_seekxl' => [
        'name' => 'seekXL',
        'description' => t('Bookmark this post on seekXL'),
        'link' => '//social-bookmarking.seekxl.de/?add_url=<encoded-url>&title=<encoded-title>',
      ],
      '_de_meinvz' => [
        'name' => 'MeinVZ',
        'description' => t('Share on MeinVZ, StudiVZ or SchuelerVZ'),
        'link' => '//www.studivz.net/Link/Selection/Url/?u=<encoded-url>&desc=<encoded-title>',
      ],
      '_de_xing' => [
        'name' => 'Xing',
        'description' => t('Share on Xing'),
        'link' => '//www.xing.com/app/user?op=share&url=<raw-encoded-url>',
      ],
    ];
  }

}
