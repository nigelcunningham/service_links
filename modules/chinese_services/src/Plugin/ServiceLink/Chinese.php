<?php

namespace Drupal\chinese_services\Plugin\ServiceLink;

use Drupal\service_links\Controller\ServiceLinksController;
use Drupal\service_links\ServiceLinkBase;

/**
 * Class Chinese.
 *
 * @ServiceLink(
 *   id = "chinese",
 *   name = @Translation("Chinese")
 * )
 */
class Chinese extends ServiceLinkBase {

  /**
   * {@inheritdoc}
   */
  public function getDetails() {
    return [
      '_cn_weibo_share_button' => [
        'name' => 'Weibo Share Button',
        'description' => t('Share at Weibo'),
        'link' => 'http://service.weibo.com/staticjs/weiboshare.html?url=<raw-encoded-url>&title=<raw-encoded-title>&count=<count>&language=<language>&type=<type>&dpc=1',
        'javascript' => 'weibo_share_button.js',
        'style' => ServiceLinksController::SERVICE_LINKS_STYLE_EMPTY,
      ],
    ];
  }

}
