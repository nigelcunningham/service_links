<?php

namespace Drupal\italian_services\Plugin\ServiceLink;

use Drupal\service_links\ServiceLinkBase;

/**
 * Class Italian.
 *
 * @ServiceLink(
 *   id = "italian",
 *   name = @Translation("Italian")
 * )
 */
class Italian extends ServiceLinkBase {

  /**
   * {@inheritdoc}
   */
  public function getDetails() {
    return [
      '_it_diggita' => [
        'name' => 'Diggita',
        'description' => t('Add a news on Diggita'),
        'link' => '//www.diggita.it/submit.php?url=<encoded-url>&title=<encoded-title>',
      ],
    ];
  }

}
