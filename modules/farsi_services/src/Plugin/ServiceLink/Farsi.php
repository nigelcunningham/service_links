<?php

namespace Drupal\farsi_services\Plugin\ServiceLink;

use Drupal\service_links\ServiceLinkBase;

/**
 * Class Farsi.
 *
 * @ServiceLink(
 *   id = "farsi",
 *   name = @Translation("Farsi")
 * )
 */
class Farsi extends ServiceLinkBase {

  /**
   * {@inheritdoc}
   */
  public function getDetails() {
    return [
      '_fa_balatarin' => [
        'name' => 'Balatarin',
        'description' => t('Bookmark this post on balatarin'),
        'link' => '//balatarin.com/links/submit?phase=2&url=<raw-encoded-url>&title=<raw-encoded-title>',
      ],
    ];
  }

}
