<?php

namespace Drupal\general_services\Plugin\ServiceLink;

;

use Drupal\service_links\Controller\ServiceLinksController;
use Drupal\service_links\ServiceLinkBase;

/**
 * Class GeneralServices.
 *
 * @ServiceLink(
 *   id = "general_services",
 *   name = @Translation("General Services")
 * )
 */
class GeneralServices extends ServiceLinkBase {

  /**
   * {@inheritdoc}
   */
  public function getDetails() {
    return [
      'blogmarks' => [
        'name' => 'Blogmarks',
        'description' => t('Mark it on Blogmarks!'),
        'link' => 'http://blogmarks.net/my/new.php?mini=1&title=<raw-encoded-title>&url=<raw-encoded-url>',
        'icon' => 'blogmarks.png',
      ],
      'blokube' => [
        'name' => 'Blokube',
        'description' => t('Blog this on Blokube'),
        'link' => 'http://www.blokube.com/submit/?url=<raw-encoded-url>&title=<raw-encoded-title>',
        'icon' => 'blokube.png',
      ],
      'citeulike' => [
        'name' => 'CiteULike',
        'description' => t('Cite it on CiteULike!'),
        'link' => 'http://www.citeulike.org/posturl?url=<raw-encoded-url>&title=<raw-encoded-title>',
        'icon' => 'citeulike.png',
      ],
      'delicious' => [
        'name' => 'del.icio.us',
        'description' => t('Bookmark this post on del.icio.us'),
        'link' => '//del.icio.us/post?url=<encoded-url>&title=<encoded-title>',
      ],
      'digg' => [
        'name' => 'Digg',
        'description' => t('Digg this post on digg.com'),
        'link' => '//digg.com/submit?phase=2&url=<encoded-url>&title=<encoded-title>',
      ],
      'fark' => [
        'name' => 'Fark',
        'description' => t('Aggregate it on Fark'),
        'link' => 'http://cgi.fark.com/cgi/fark/submit.pl?new_url=<raw-encoded-url>',
        'icon' => 'fark.png',
      ],
      'stumbleupon' => [
        'name' => 'StumbleUpon',
        'description' => t('Thumb this up at StumbleUpon'),
        'link' => '//www.stumbleupon.com/submit?url=<encoded-url>&title=<encoded-title>',
        'icon' => 'stumbleit.png',
      ],
      'stumbleupon_badge' => [
        'name' => 'Stumbleupon Badge',
        'description' => t('Stumble it'),
        'link' => 'http://www.stumbleupon.com/submit?url=<raw-encoded-url>',
        'javascript' => 'stumbleupon_badge.js',
        'style' => ServiceLinksController::SERVICE_LINKS_STYLE_EMPTY,
        'icon' => 'stumbleupon_badge.png',
      ],
      'twitter' => [
        'name' => 'Twitter',
        'description' => t('Share this on Twitter'),
        'link' => '//twitter.com/share?url=<raw-encoded-short-url>&text=<raw-encoded-title>',
      ],
      'reddit' => [
        'name' => 'Reddit',
        'link' => '//reddit.com/submit?url=<encoded-url>&title=<encoded-title>',
        'description' => t('Submit this post on reddit.com'),
      ],
      'reddit_counter_button' => [
        'name' => 'Reddit Counter Button',
        'description' => t('Reddit this'),
        'link' => 'http://www.reddit.com/submit?url=<raw-encoded-url>&title=<raw-encoded-title>&target=<target>&bgcolor=<bgcolor>&bordercolor=<bordercolor>',
        'javascript' => 'reddit_counter_button.js',
        'style' => ServiceLinksController::SERVICE_LINKS_STYLE_EMPTY,
      ],
      'facebook' => [
        'name' => 'Facebook',
        'link' => '//www.facebook.com/sharer.php?u=<encoded-url>&t=<encoded-title>',
        'description' => t('Share on Facebook'),
      ],
      'myspace' => [
        'name' => 'MySpace',
        'link' => '//www.myspace.com/index.cfm?fuseaction=postto&t=<encoded-title>&u=<encoded-url>',
        'description' => t('Share on MySpace'),
      ],
      'google' => [
        'name' => 'Google',
        'link' => '//www.google.com/bookmarks/mark?op=add&bkmk=<encoded-url>&title=<encoded-title>',
        'description' => t('Bookmark this post on Google'),
      ],
      'google_plus' => [
        'name' => 'Google+',
        'link' => '//plus.google.com/share?url=<encoded-url>',
        'description' => t('Share this on Google+'),
      ],
      'google_translate' => [
        'name' => 'Google Translate',
        'description' => t('Translate it on Google Translate'),
        'link' => 'http://translate.google.com/translate?sl=auto&u=<raw-encoded-url>',
      ],
      'instapaper' => [
        'name' => 'Instapaper',
        'description' => t('Save this for later with Instapaper'),
        'link' => 'http://www.instapaper.com/hello2?url=<raw-encoded-url>&title=<raw-encoded-title>&description=<raw-encoded-teaser>',
      ],
      'linkedin' => [
        'name' => 'LinkedIn',
        'link' => '//www.linkedin.com/shareArticle?mini=true&url=<encoded-url>&title=<encoded-title>&summary=<encoded-teaser>&source=<encoded-source>',
        'description' => t('Publish this post to LinkedIn'),
      ],
      'linkagogo' => [
        'name' => 'LinkaGoGo',
        'description' => t('Link it a GoGo'),
        'link' => 'http://www.linkagogo.com/go/AddNoPopup?title=<raw-encoded-title>&url=<raw-encoded-url>',
      ],
      'live' => [
        'name' => 'Live',
        'description' => t('Add to Windows Live favorites'),
        'link' => 'http://favorites.live.com/quickadd.aspx?url=<raw-encoded-url>&title=<raw-encoded-title>&text=<raw-encoded-description>',
        'icon' => 'livejournal.png',
      ],
      'livejournal' => [
        'name' => 'LiveJournal',
        'description' => t('Blog it on LiveJournal!'),
        'link' => 'http://www.livejournal.com/update.bml?subject=<raw-encoded-title>&event=<raw-encoded-url>',
      ],
      'box' => [
        'name' => 'Box',
        'link' => '//www.box.net/api/1.0/import?url=<encoded-url>&name=<encoded-title>&description=<encoded-teaser>&import_as=link',
        'description' => t('Box it!'),
      ],
      'blinklist' => [
        'name' => 'Blinklist',
        'link' => '//www.blinklist.com/index.php?Action=Blink/addblink.php&Description=<encoded-teaser>&Url=<encoded-url>&Title=<encoded-title>&Pop=yes',
        'description' => t('Add to Blinklist'),
      ],
      'identica' => [
        'name' => 'identi.ca',
        'link' => '//identi.ca/?action=newnotice&status_textarea=<encoded-title> <encoded-url>',
        'description' => t('Dent this on identi.ca'),
      ],
      'diigo' => [
        'name' => 'Diigo',
        'description' => t('Post on Diigo'),
        'link' => '//www.diigo.com/post?url=<encoded-url>&title=<encoded-title>&desc=<encoded-teaser>',
      ],
      'viadeo' => [
        'name' => 'Viadeo',
        'link' => '//www.viadeo.com/shareit/share/?url=<encoded-url>&title=<encoded-title>',
        'description' => t('Share this on Viadeo'),
      ],
      'netvouz' => [
        'name' => 'Netvouz',
        'description' => t('Bookmark on Netvouz'),
        'link' => 'http://www.netvouz.com/action/submitBookmark?url=<raw-encoded-url>&title=<raw-encoded-title>',
      ],
      'plurk' => [
        'name' => 'Plurk',
        'description' => t('Say it on Plurk'),
        'link' => 'http://www.plurk.com/m?content=<raw-encoded-title>%20-%20<raw-encoded-short-url>&qualifier=shares',
      ],
      'printfriendly' => [
        'name' => 'PrintFriendly',
        'description' => t('Print on PrintFriendly'),
        'link' => 'http://www.printfriendly.com/print?url=<raw-encoded-url>',
      ],
      'tumblr' => [
        'name' => 'Tumblr',
        'description' => t('Share on Tumblr'),
        'link' => 'http://www.tumblr.com/share/link?url=<encoded-url>&name=<encoded-title>&description=<encoded-teaser>',
      ],
      'urlcapt' => [
        'name' => 'URLCapt',
        'description' => t('Create a snapshot with URLCapt'),
        'link' => 'http://urlcapt.com/?url=<raw-encoded-url>',
      ],
      'web2pdf' => [
        'name' => 'Web2PDF',
        'description' => t('Download as PDF'),
        'link' => 'http://www.web2pdfconvert.com/convert.aspx?cURL=<raw-encoded-url>&title=<raw-encoded-title>',
      ],
    ];
  }

}
