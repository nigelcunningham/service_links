<?php

namespace Drupal\russian_services\Plugin\ServiceLink;

use Drupal\service_links\ServiceLinkBase;
use Drupal\service_links\Controller\ServiceLinksController;

/**
 * Class Russian.
 *
 * @ServiceLink(
 *   id = "russian",
 *   name = @Translation("Russian")
 * )
 */
class Russian extends ServiceLinkBase {

  /**
   * {@inheritdoc}
   */
  public function getDetails() {
    return [
      '_ru_ruspace' => [
        'link' => '//www.ruspace.ru/index.php?link=bookmark&action=bookmarkNew&bm=1&url=<encoded-url>&title=<encoded-title>',
        'name' => 'Ruspace',
        'description' => t('Bookmark this post on Ruspace'),
      ],
      '_ru_memori' => [
        'link' => 'http://memori.ru/link/?sm=1&u_data[url]=<encoded-url>&u_data[name]=<encoded-title>',
        'name' => 'Memori',
        'description' => t('Bookmark this post on Memori'),
      ],
      '_ru_linkstore' => [
        'link' => '//www.linkstore.ru/servlet/LinkStore?a=add&url=<encoded-url>&title=<encoded-title>',
        'name' => 'LinkStore',
        'description' => t('Bookmark this post on LinkStore'),
      ],
      '_ru_vkontakte' => [
        'link' => '//vkontakte.ru/share.php?url=<encoded-url>&title=<encoded-title>',
        'name' => 'VKontakte',
        'description' => t('Share this on VKontakte'),
      ],
      '_ru_mail_ru_share_button' => [
        'name' => 'Mail Ru Share Button',
        'description' => t('Share on Mail Ru'),
        'link' => 'http://connect.mail.ru/share?url=<raw-encoded-url>&title=<raw-encoded-title>&description=<raw-encoded-teaser>',
        'javascript' => 'mail_ru_share_button.js',
        'style' => ServiceLinksController::SERVICE_LINKS_STYLE_EMPTY,
      ],
      '_ru_odnoklassniki' => [
        'name' => 'Odnoklassniki',
        'description' => t('Share this post on Odnoklassniki'),
        'link' => 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1&st._surl=<raw-encoded-url>',
        'icon' => '_ru_odnoklassniki_share_button.png',
      ],
      '_ru_vk_share_button' => [
        'name' => 'VK Share Button',
        'description' => t('Share it on VK'),
        'link' => 'http://vkontakte.ru/share.php?url=<raw-encoded-url>',
        'javascript' => 'vk_share_button.js',
        'style' => ServiceLinksController::SERVICE_LINKS_STYLE_EMPTY,
        'css' => 'vk_share_button.css',
      ],
    ];
  }

}
