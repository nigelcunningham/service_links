<?php

namespace Drupal\swedish_services\Plugin\ServiceLink;

use Drupal\service_links\ServiceLinkBase;

/**
 * Class Swedish.
 *
 * @ServiceLink(
 *   id = "swedish",
 *   name = @Translation("Swedish")
 * )
 */
class Swedish extends ServiceLinkBase {

  /**
   * {@inheritdoc}
   */
  public function getDetails() {
    return [
      '_se_pusha' => [
        'name' => 'Pusha',
        'description' => t('Share this on Pusha'),
        'link' => '//pusha.se/posta?url=<encoded-url>&title=<encoded-title>',
      ],
    ];
  }

}
