<?php

namespace Drupal\french_services\Plugin\ServiceLink;

use Drupal\service_links\ServiceLinkBase;

/**
 * Class French.
 *
 * @ServiceLink(
 *   id = "french",
 *   name = @Translation("French")
 * )
 */
class French extends ServiceLinkBase {

  /**
   * {@inheritdoc}
   */
  public function getDetails() {
    return [
      '_fr_skyrock' => [
        'name' => 'Skyrock',
        'description' => t('Blog it on Skyrock!'),
        'link' => 'http://www.skyrock.com/m/blog/share.php?url=<raw-encoded-url>&title=<raw-encoded-title>',
      ],
    ];
  }

}
