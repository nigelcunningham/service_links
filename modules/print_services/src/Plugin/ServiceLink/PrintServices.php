<?php

namespace Drupal\print_services\Plugin\ServiceLink;

use Drupal\service_links\ServiceLinkBase;

/**
 * Class PrintServices.
 *
 * @ServiceLink(
 *   id = "print_services",
 *   name = @Translation("Print Services")
 * )
 */
class PrintServices extends ServiceLinkBase {

  /**
   * {@inheritdoc}
   */
  public function getDetails() {
    $links = [
      'print' => [
        'name' => 'Print HTML',
        'link' => '<entity-type>/<entity-id>/printable/print',
        'description' => t('Printable version'),
        'internal' => TRUE,
        'access' => 'isVisible',
      ],
    ];

    if (\Drupal::moduleHandler()->moduleExists('printable_pdf')) {
      $links['printpdf'] = [
        'name' => 'Print PDF',
        'link' => '<entity-type>/<entity-id>/printable/pdf',
        'description' => t('PDF version'),
        'internal' => TRUE,
        'access' => 'isVisible',
      ];
    }

    if (\Drupal::moduleHandler()->moduleExists('print_mail')) {
      $links['printmail'] = [
        'name' => 'Print Mail',
        'link' => '<front-page>printmail/<query>',
        'description' => t('Send to a friend'),
        'internal' => TRUE,
        'access' => 'isVisible',
      ];
    }

    return $links;
  }

}
