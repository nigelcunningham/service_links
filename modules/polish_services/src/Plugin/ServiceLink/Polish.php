<?php

namespace Drupal\polish_services\Plugin\ServiceLink;

use Drupal\service_links\ServiceLinkBase;

/**
 * Class Polish.
 *
 * @ServiceLink(
 *   id = "polish",
 *   name = @Translation("Polish")
 * )
 */
class Polish extends ServiceLinkBase {

  /**
   * {@inheritdoc}
   */
  public function getDetails() {
    return [
      '_pl_wykop' => [
        'name' => 'Wykop',
        'description' => t('Add to Wykop'),
        'link' => '//www.wykop.pl/dodaj?url=<raw-encoded-url>&title=<raw-encoded-title>',
      ],
      '_pl_blip' => [
        'name' => 'Blip',
        'description' => t('Add to Blip'),
        'link' => '//blip.pl/dashboard?body=<raw-encoded-short-url>%20--%20<raw-encoded-title>',
      ],
    ];
  }

}
